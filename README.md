# ParkTrackr

ParkTrackr is an amusement park platform on which the experience of visiting, experiencing and the excitement for amusement park is being enhanced.

### Installation

ParkTrackr requires [Node.js](https://nodejs.org/) v4+ to run.

Install the dependencies and and start the server.

```sh
$ cd partktrackr
$ npm install
$ npm start
```

### Tech

ParkTrackr uses a number of open source projects to work properly:

* [Apollo] - combining APIs, databases, and microservices into a single data graph that you can query with GraphQL
* [Express] - fast node.js network app framework 
* [GraphQL] - query language for APIs
* [MongoDB] - a document database
* [mongoose] - elegant mongodb object modeling for node.js
* [node.js] - evented I/O for the backend
* [ReactJS] - HTML enhanced for web apps!





### Packages

ParkTrackr is currently extended with the following packages. Instructions on how to use them in your own development are linked below.

| Plugin | README |
| ------ | ------ |
| @apollo/react-hooks | [https://www.apollographql.com/docs/react/api/react-hooks/] |
| moment | [https://momentjs.com/docs/] |
| react-apollo | [https://www.apollographql.com/docs/react/api/react-apollo/] |
| react-helmet | [https://github.com/nfl/react-helmet] |
| react-share | [https://github.com/nygardk/react-share] |



### Development

Want to contribute? Great!
Contact us.




