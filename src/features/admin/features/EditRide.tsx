import React, { useState } from 'react';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import { useMutation, useQuery } from '@apollo/react-hooks';
import Select from 'react-select';

// mutations
import { convertToSelect } from '../../../utils/helper';
import { UPDATE_RIDE } from '../../../apollo/graphql/mutations/ride';
import {
	RIDE_QUERY_BY_ID,
	RIDE_CATEGORIES_QUERY,
	RIDE_TYPES_QUERY,
} from '../../../apollo/graphql/queries/ride';
import { PARKS_QUERY } from '../../../apollo/graphql/queries/park';

const intensityOptions = [
	{ label: 'Kids', value: '0' },
	{ label: 'Family', value: '1' },
	{ label: 'Thrill', value: '2' },
	{ label: 'Extreme', value: '3' },
];

const EditRide: React.FC = ({ match }: any) => {
	const { params } = match;
	const { Id } = params;
	const [name, setName] = useState('');
	const [park, setPark] = useState({
		label: '',
		value: '',
	});
	const [categories, setCategories] = useState([]);
	const [types, setTypes] = useState([]);
	const [intensity, setIntensity] = useState({
		label: '',
		value: '',
	});
	const [tempImage, setTempImage] = useState('');
	const [height, setHeight] = useState('');
	const [length, setLength] = useState('');

	const { error: rideQueryError, data: rideData } = useQuery<any>(
		RIDE_QUERY_BY_ID,
		{
			variables: { id: Id },
			onCompleted() {
				const { ride } = rideData;
				const {
					originalName,
					tempImage: temp,
					park: parkData,
					intensity: intensityData,
					rideCategories,
					rideTypes,
				} = ride;
				const mappedRideCategories =
					rideCategories &&
					rideCategories.map((cat: any) => ({
						label: cat.originalName,
						value: cat.id,
					}));
				const mappedRideTypes =
					rideTypes &&
					rideTypes.map((type: any) => ({
						label: type.originalName,
						value: type.id,
					}));
				setName(originalName);
				setPark({
					label: parkData.originalName,
					value: parkData.id,
				});
				if (ride.intensity !== null) {
					setIntensity({
						label: intensityOptions[intensityData].label,
						value: `${ride.intensity}`,
					});
				}
				if (ride.height !== null) {
					setHeight(ride.height);
				}

				setCategories(mappedRideCategories);
				setTypes(mappedRideTypes);
				setTempImage(temp);
			},
			onError() {
				console.error(rideQueryError);
			},
		}
	);

	const {
		loading: parksQueryLoading,
		error: parksQueryError,
		data: parksData,
	} = useQuery<any>(PARKS_QUERY, {
		onError() {
			console.error(parksQueryError);
		},
	});

	const {
		loading: rideCategoriesQueryLoading,
		error: rideCategoriesQueryError,
		data: rideCategoriesQueryData,
	} = useQuery<any>(RIDE_CATEGORIES_QUERY, {
		onError() {
			console.error(rideCategoriesQueryError);
		},
	});

	const {
		loading: rideTypesQueryLoading,
		error: rideTypesQueryError,
		data: rideTypesQueryData,
	} = useQuery<any>(RIDE_TYPES_QUERY, {
		onError() {
			console.error(rideTypesQueryError);
		},
	});

	const [updateRide] = useMutation(UPDATE_RIDE, {
		onError(err) {
			console.error(err);
		},
	});

	const handleSubmit = (e: any) => {
		e.preventDefault();
		updateRide({
			variables: {
				id: Id,
				name,
				parkId: park.value,
				intensity: parseInt(intensity.value, 10),
				rideCategoryIds: categories
					? categories.map((obj: any) => obj.value)
					: [],
				rideTypeIds: types ? types.map((obj: any) => obj.value) : [],
				height: parseInt(height, 10),
				length: parseInt(length, 10),
				tempImage,
			},
		});
	};

	if (
		parksQueryLoading ||
		rideCategoriesQueryLoading ||
		rideTypesQueryLoading
	)
		return <p>Loading...</p>;

	const { parks } = parksData;
	const { rideCategories } = rideCategoriesQueryData;
	const { rideTypes } = rideTypesQueryData;
	return (
		<div css={AddParkContainer}>
			<h2>Add Park</h2>
			<p>Add a new Park to ParkTrackr</p>
			<form css={formStyle} onSubmit={(e: any) => handleSubmit(e)}>
				<label htmlFor="name" css={labelStyle}>
					Name (required)
					<input
						type="text"
						id="name"
						name="name"
						value={name}
						onChange={(e: any) => {
							setName(e.target.value);
						}}
						css={inputField}
					/>
				</label>
				<p>Park (required)</p>
				<Select
					options={convertToSelect(parks, 'id', 'originalName')}
					value={park}
					onChange={(e: any) => {
						setPark(e);
					}}
				/>
				<p>Intensity </p>
				<Select
					options={intensityOptions}
					value={intensity}
					onChange={(e: any) => {
						setIntensity(e);
					}}
				/>
				<p>Category or Categories (required)</p>
				<Select
					options={convertToSelect(
						rideCategories,
						'id',
						'originalName'
					)}
					value={categories}
					isMulti
					onChange={(e: any) => {
						setCategories(e);
					}}
				/>
				<label htmlFor="height" css={labelStyle}>
					Height in meters
					<input
						type="number"
						id="height"
						name="height"
						defaultValue={height}
						pattern="[0-9]{0,5}"
						onInput={(e: any) => {
							setHeight(e.target.value);
						}}
						css={inputField}
					/>
				</label>
				<label htmlFor="length" css={labelStyle}>
					Length in meters
					<input
						type="number"
						id="length"
						name="length"
						defaultValue={length}
						pattern="[0-9]{0,5}"
						onInput={(e: any) => {
							setLength(e.target.value);
						}}
						css={inputField}
					/>
				</label>
				<p>Type or Tyoes (required)</p>
				<Select
					options={convertToSelect(rideTypes, 'id', 'originalName')}
					value={types}
					isMulti
					onChange={(e: any) => {
						setTypes(e);
					}}
				/>
				<label htmlFor="tempImage" css={labelStyle}>
					Temporary Image URL (required)
					<input
						type="text"
						id="tempImage"
						name="tempImage"
						value={tempImage}
						onChange={(e: any) => {
							setTempImage(e.target.value);
						}}
						css={inputField}
					/>
				</label>
				<input type="submit" value="Submit" />
			</form>
		</div>
	);
};

const AddParkContainer = css`
	padding: 0 16px;
`;

const formStyle = css`
	display: flex;
	flex-direction: column;
`;

const labelStyle = css`
	display: flex;
	flex-direction: column;
	padding-bottom: 16px;
`;

const inputField = css`
	box-sizing: border-box;
	width: 100%;
	border-style: solid;
	border-width: 1px;
	padding: 3px 10px;
	min-height: 40px;
	line-height: 2;
`;
export default EditRide;
