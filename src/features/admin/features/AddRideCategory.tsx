import React, { useState } from 'react';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import { useMutation } from '@apollo/react-hooks';
import { SketchPicker } from 'react-color';

// mutations
import { CREATE_RIDE_CATEGORY } from '../../../apollo/graphql/mutations/ride';

const AddRideCategory: React.FC = () => {
	const [name, setName] = useState('');
	const [color, setColor] = useState('');

	const [addAchievement] = useMutation(CREATE_RIDE_CATEGORY, {
		variables: {
			name,
			color,
		},
		onError(err) {
			console.error(err);
		},
	});

	const handleSubmit = (e: any) => {
		e.preventDefault();
		addAchievement();
	};

	return (
		<div css={AddParkContainer}>
			<h2>Add Ride Category</h2>
			<p>Add a new Ride Category to ParkTrackr</p>
			<form css={formStyle} onSubmit={(e: any) => handleSubmit(e)}>
				<label htmlFor="name" css={labelStyle}>
					Name (required)
					<input
						type="text"
						id="name"
						name="name"
						value={name}
						onChange={(e: any) => {
							setName(e.target.value);
						}}
						css={inputField}
					/>
				</label>
				<p css={labelStyle}>Color</p>
				<div>
					<SketchPicker
						width="90%"
						disableAlpha
						color={color || '#000000'}
						onChangeComplete={(e: any) => setColor(e.hex)}
					/>
				</div>

				<input type="submit" value="Submit" />
			</form>
		</div>
	);
};

const AddParkContainer = css`
	padding: 0 16px;
`;

const formStyle = css`
	display: flex;
	flex-direction: column;
`;

const labelStyle = css`
	display: flex;
	flex-direction: column;
	padding-bottom: 16px;
	z-index: 8;
`;

const inputField = css`
	box-sizing: border-box;
	width: 100%;
	border-style: solid;
	border-width: 1px;
	padding: 3px 10px;
	min-height: 40px;
	line-height: 2;
	z-index: 8;
`;

export default AddRideCategory;
