import React, { useState } from 'react';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import { useMutation } from '@apollo/react-hooks';

// mutations
import { CREATE_PARK } from '../../../apollo/graphql/mutations/park';

const AddPark: React.FC = () => {
	const [name, setName] = useState('');
	const [tempImage, setTempImage] = useState('');

	const [addPark] = useMutation(CREATE_PARK, {
		variables: { name, tempImage },
		onError(err) {
			console.error(err);
		},
	});

	const handleSubmit = (e: any) => {
		e.preventDefault();
		addPark();
	};

	return (
		<div css={AddParkContainer}>
			<h2>Add Park</h2>
			<p>Add a new Park to ParkTrackr</p>
			<form css={formStyle} onSubmit={(e: any) => handleSubmit(e)}>
				<label htmlFor="name" css={labelStyle}>
					Name (required)
					<input
						type="text"
						id="name"
						name="name"
						value={name}
						onChange={(e: any) => {
							setName(e.target.value);
						}}
						css={inputField}
					/>
				</label>
				<label htmlFor="tempImage" css={labelStyle}>
					Temporary Image URL (required)
					<input
						type="text"
						id="tempImage"
						name="tempImage"
						value={tempImage}
						onChange={(e: any) => {
							setTempImage(e.target.value);
						}}
						css={inputField}
					/>
				</label>
				<input type="submit" value="Submit" />
			</form>
		</div>
	);
};

const AddParkContainer = css`
	padding: 0 16px;
`;

const formStyle = css`
	display: flex;
	flex-direction: column;
`;

const labelStyle = css`
	display: flex;
	flex-direction: column;
	padding-bottom: 16px;
	z-index: 8;
`;

const inputField = css`
	box-sizing: border-box;
	width: 100%;
	border-style: solid;
	border-width: 1px;
	padding: 3px 10px;
	min-height: 40px;
	line-height: 2;
	z-index: 8;
`;
export default AddPark;
