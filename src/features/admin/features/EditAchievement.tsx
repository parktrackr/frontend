import React, { useState } from 'react';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import { useMutation, useQuery } from '@apollo/react-hooks';
import Select from 'react-select';
import { SketchPicker } from 'react-color';

import { ACHIEVEMENT_QUERY_BY_ID } from '../../../apollo/graphql/queries/achievement';

// mutations
import { UPDATE_ACHIEVEMENT } from '../../../apollo/graphql/mutations/achievement';

import { RIDES_QUERY } from '../../../apollo/graphql/queries/ride';

import { convertToSelect } from '../../../utils/helper';

const AddAchievement: React.FC = ({ match }: any) => {
	const { params } = match;
	const { Id } = params;
	const [name, setName] = useState('');
	const [rideIds, setRideIds] = useState([]);
	const [color, setColor] = useState('');
	const [icon, setIcon] = useState('');

	const {
		error: achievementQueryError,
		data: achievementQueryData,
	} = useQuery<any>(ACHIEVEMENT_QUERY_BY_ID, {
		variables: { id: Id },
		onCompleted() {
			const { achievement } = achievementQueryData;
			const {
				color: colorData,
				icon: iconData,
				conditions,
				translation,
			} = achievement;
			const { name: nameData } = translation;

			const mappedConditionss = conditions.map((cond: any) => ({
				label: cond.ride.originalName,
				value: cond.ride.id,
			}));
			setName(nameData);
			setColor(colorData);
			setRideIds(mappedConditionss);
			setIcon(iconData);
		},
		onError() {
			console.error(achievementQueryError);
		},
	});

	const [addAchievement] = useMutation(UPDATE_ACHIEVEMENT, {
		onError(err) {
			console.error(err);
		},
	});

	const handleSubmit = (e: any) => {
		e.preventDefault();
		addAchievement({
			variables: {
				id: Id,
				name,
				color,
				icon,
				rideIds: rideIds && rideIds.map((obj: any) => obj.value),
			},
		});
	};

	const {
		loading: ridesQueryLoading,
		error: ridesQueryError,
		data: ridesData,
	} = useQuery<any>(RIDES_QUERY, {
		onError() {
			console.error(ridesQueryError);
		},
	});

	if (ridesQueryLoading) return <p>Loading...</p>;

	const { rides } = ridesData;
	return (
		<div css={AddParkContainer}>
			<h2>Update Achievement</h2>
			<p>Update an Achievement</p>
			<form css={formStyle} onSubmit={(e: any) => handleSubmit(e)}>
				<label htmlFor="name" css={labelStyle}>
					Name (required)
					<input
						type="text"
						id="name"
						name="name"
						value={name}
						onChange={(e: any) => {
							setName(e.target.value);
						}}
						css={inputField}
					/>
				</label>
				<p>Ride or Rides (required)</p>
				<Select
					css={selectStyle}
					options={convertToSelect(rides, 'id', 'originalName')}
					value={rideIds}
					isMulti
					onChange={(e: any) => {
						setRideIds(e);
					}}
				/>
				<p css={labelStyle}>Color</p>
				<div>
					<SketchPicker
						width="90%"
						disableAlpha
						color={color || '#000000'}
						onChangeComplete={(e: any) => setColor(e.hex)}
					/>
				</div>
				<label htmlFor="icon" css={labelStyle}>
					Icon (required)
					<input
						type="text"
						id="icon"
						name="icon"
						value={icon}
						onChange={(e: any) => {
							setIcon(e.target.value);
						}}
						css={inputField}
					/>
				</label>
				<input type="submit" value="Submit" />
			</form>
		</div>
	);
};

const AddParkContainer = css`
	padding: 0 16px;
`;

const formStyle = css`
	display: flex;
	flex-direction: column;
`;

const labelStyle = css`
	display: flex;
	flex-direction: column;
	padding-bottom: 16px;
	z-index: 8;
`;

const inputField = css`
	box-sizing: border-box;
	width: 100%;
	border-style: solid;
	border-width: 1px;
	padding: 3px 10px;
	min-height: 40px;
	line-height: 2;
	z-index: 8;
`;

const selectStyle = css`
	z-index: 10;
`;
export default AddAchievement;
