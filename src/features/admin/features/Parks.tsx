import React, { useState } from 'react';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import { useQuery, useMutation } from '@apollo/react-hooks';
import { useTable, useSortBy } from 'react-table';
import { Link } from 'react-router-dom';
import { PARKS_QUERY } from '../../../apollo/graphql/queries/park';
import { DELETE_PARK } from '../../../apollo/graphql/mutations/park';

const Parks = () => {
	const [active, setActive] = useState('');
	const {
		loading: parksQueryLoading,
		error: parksQueryError,
		data: parksData,
	} = useQuery<any>(PARKS_QUERY, {
		onError() {
			console.error(parksQueryError);
		},
	});

	const [deletePark] = useMutation(DELETE_PARK, {
		onError(err) {
			console.error(err);
		},
	});

	const data = React.useMemo(() => {
		if (!parksQueryLoading) {
			return parksData.parks;
		}
		return [];
	}, [parksData, parksQueryLoading]);
	const columns = React.useMemo(
		() => [
			{
				Header: 'Name',
				accessor: 'originalName',
				id: 'name',
				Cell: ({ cell }: any) => {
					const { row } = cell;
					const { original } = row;
					const { id, originalName, slug, rides } = original;
					const displayRides =
						rides.length > 0 ? (
							rides &&
							rides.map((val: any) => {
								return (
									<p key={val.id} css={subInformationText}>
										{val.originalName}
									</p>
								);
							})
						) : (
							<p css={subInformationText}>No Rides</p>
						);
					return (
						<div css={cellStyle}>
							<div css={titleContainer}>
								<p css={cellTitle}>{originalName}</p>
								<div
									onClick={() =>
										active === id
											? setActive('')
											: setActive(id)
									}
									onKeyDown={() =>
										active === id
											? setActive('')
											: setActive(id)
									}
									tabIndex={-1}
									role="button"
									aria-label="overlayClickable"
								>
									{// eslint-disable-next-line no-nested-ternary
									active === id ? '🔽' : '🔼'}
								</div>
							</div>
							{active === id ? (
								<div css={subInformation}>
									<div css={subInformationHeader}>
										<p css={subInformationText}>Rides</p>
									</div>
									<div css={subInformationHeaderTwo}>
										{displayRides}
									</div>
								</div>
							) : null}
							<div css={optionsContainer}>
								<Link to={`/park/${slug}`}>View</Link> |{' '}
								<Link to={`/admin/parks/edit/${id}`}>Edit</Link>{' '}
								|{' '}
								<button
									type="button"
									onClick={() =>
										deletePark({ variables: { id } })
									}
								>
									Delete
								</button>
							</div>
						</div>
					);
				},
			},
		],
		[active, deletePark]
	);

	const {
		getTableProps,
		getTableBodyProps,
		headerGroups,
		rows,
		prepareRow,
	} = useTable(
		{
			columns,
			data,
			initialState: {
				sortBy: [{ id: 'name', desc: false }],
			},
		},
		useSortBy
	);

	if (parksQueryLoading) return <p>Loading..</p>;

	return (
		<div css={pageWrapper}>
			<h2>Parks</h2>
			<table {...getTableProps()} css={tableStyle}>
				<thead css={tableHeadStyle}>
					{headerGroups.map(headerGroup => (
						<tr {...headerGroup.getHeaderGroupProps()}>
							{headerGroup.headers.map(column => (
								<th
									css={THStyle}
									{...column.getHeaderProps(
										column.getSortByToggleProps()
									)}
								>
									{column.render('Header')}
									<span>
										{// eslint-disable-next-line no-nested-ternary
										column.isSorted
											? column.isSortedDesc
												? '  🔽'
												: '  🔼'
											: ''}
									</span>
								</th>
							))}
						</tr>
					))}
				</thead>
				<tbody {...getTableBodyProps()} css={tableBodyStyle}>
					{rows.map(row => {
						prepareRow(row);
						return (
							<tr {...row.getRowProps()}>
								{row.cells.map(cell => {
									return (
										<td
											css={TDStyle}
											{...cell.getCellProps()}
										>
											{cell.render('Cell')}
										</td>
									);
								})}
							</tr>
						);
					})}
				</tbody>
			</table>
		</div>
	);
};

const pageWrapper = css`
	padding: 8px;
`;

const tableStyle = css`
	border: 2px solid grey;
	width: 100%;
`;

const tableHeadStyle = css`
	border: 2px solid grey;
`;

const tableBodyStyle = css`
	border: 2px solid grey;
`;

const THStyle = css`
	border: 2px solid grey;
`;

const TDStyle = css`
	border: 2px solid grey;
`;

const cellStyle = css`
	width: 100%;
	padding: 0 8px;
	box-sizing: border-box;
`;

const cellTitle = css`
	margin: 8px 0;
	font-weight: 700;
`;

const subInformation = css`
	display: flex;
	flex-direction: row;
	width: 100%;
`;
const subInformationHeader = css`
	display: flex;
	flex-direction: column;
	width: 30%;
`;

const subInformationHeaderTwo = css`
	display: flex;
	flex-direction: column;
	width: 70%;
`;

const subInformationText = css`
	margin: 8px 0;
`;

const titleContainer = css`
	display: flex;
	flex-direction: row;
	width: 100%;
	justify-content: space-between;
`;

const optionsContainer = css`
	width: 50%;
	display: flex;
	flex-direction: row;
	padding: 8px 0;
	justify-content: space-between;
`;

export default Parks;
