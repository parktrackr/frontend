import React, { useState } from 'react';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import { useMutation, useQuery } from '@apollo/react-hooks';
import Select from 'react-select';

// mutations
import { CREATE_RIDE } from '../../../apollo/graphql/mutations/ride';

import { PARKS_QUERY } from '../../../apollo/graphql/queries/park';
import {
	RIDE_CATEGORIES_QUERY,
	RIDE_TYPES_QUERY,
} from '../../../apollo/graphql/queries/ride';

import { convertToSelect } from '../../../utils/helper';

const AddRide: React.FC = () => {
	const [name, setName] = useState('');
	const [parkId, setParkId] = useState({
		label: '',
		value: '',
	});
	const [categoryIds, setCategoryIds] = useState([]);
	const [typeIds, setTypeIds] = useState([]);
	const [tempImage, setTempImage] = useState('');

	const [addRide] = useMutation(CREATE_RIDE, {
		variables: {
			name,
			parkId: parkId.value,
			tempImage,
			rideCategoryIds: categoryIds.map((obj: any) => obj.value),
			rideTypeIds: typeIds.map((obj: any) => obj.value),
		},
		onError(err) {
			console.error(err);
		},
	});

	const {
		loading: parksQueryLoading,
		error: parksQueryError,
		data: parksData,
	} = useQuery<any>(PARKS_QUERY, {
		onError() {
			console.error(parksQueryError);
		},
	});

	const {
		loading: rideCategoriesQueryLoading,
		error: rideCategoriesQueryError,
		data: rideCategoriesData,
	} = useQuery<any>(RIDE_CATEGORIES_QUERY, {
		onError() {
			console.error(rideCategoriesQueryError);
		},
	});

	const {
		loading: rideTypesQueryLoading,
		error: rideTypesQueryError,
		data: rideTypesData,
	} = useQuery<any>(RIDE_TYPES_QUERY, {
		onError() {
			console.error(rideTypesQueryError);
		},
	});

	const handleSubmit = (e: any) => {
		e.preventDefault();
		addRide();
	};

	if (
		parksQueryLoading ||
		rideCategoriesQueryLoading ||
		rideTypesQueryLoading
	)
		return <p>Loading...</p>;
	const { parks } = parksData;
	const { rideCategories } = rideCategoriesData;
	const { rideTypes } = rideTypesData;

	return (
		<div css={AddRideContainer}>
			<h2>Add Ride</h2>
			<p>Add a new Ride to ParkTrackr</p>
			<form css={formStyle} onSubmit={handleSubmit}>
				<label htmlFor="name" css={labelStyle}>
					Name (required)
					<input
						type="text"
						id="name"
						name="name"
						value={name}
						onChange={(e: any) => {
							setName(e.target.value);
						}}
						css={inputField}
					/>
				</label>
				<p>Park (required)</p>
				<Select
					options={convertToSelect(parks, 'id', 'originalName')}
					value={parkId}
					onChange={(e: any) => {
						setParkId(e);
					}}
				/>
				<p>Category or Categories (required)</p>
				<Select
					options={convertToSelect(
						rideCategories,
						'id',
						'originalName'
					)}
					value={categoryIds}
					isMulti
					onChange={(e: any) => {
						setCategoryIds(e);
					}}
				/>
				<p>Type or Types (required)</p>
				<Select
					options={convertToSelect(rideTypes, 'id', 'originalName')}
					value={typeIds}
					isMulti
					onChange={(e: any) => {
						setTypeIds(e);
					}}
				/>
				<label htmlFor="tempImage" css={labelStyle}>
					Temporary Image URL (required)
					<input
						type="text"
						id="tempImage"
						name="tempImage"
						value={tempImage}
						onChange={(e: any) => {
							setTempImage(e.target.value);
						}}
						css={inputField}
					/>
				</label>
				<input type="submit" value="Submit" />
			</form>
		</div>
	);
};

const AddRideContainer = css`
	padding: 0 16px;
`;

const formStyle = css`
	display: flex;
	flex-direction: column;
`;

const labelStyle = css`
	display: flex;
	flex-direction: column;
	padding-bottom: 16px;
`;

const inputField = css`
	box-sizing: border-box;
	width: 100%;
	border-style: solid;
	border-width: 1px;
	padding: 3px 10px;
	min-height: 40px;
	line-height: 2;
`;

export default AddRide;
