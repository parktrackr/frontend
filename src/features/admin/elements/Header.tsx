import React, { useState } from 'react';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */

import { Link } from 'react-router-dom';
import Sidebar from './Sidebar';

import menuIcon from '../../../assets/icons/menu.svg';
import homeIcon from '../../../assets/icons/home.svg';
import addIcon from '../../../assets/icons/add.svg';
import userIcon from '../../../assets/icons/user.svg';

const AddMenu = ({ handleAddMenu }: any) => (
	<div css={addMenuStyle}>
		<Link onClick={handleAddMenu} css={addMenuLink} to="/admin/parks/add">
			Park
		</Link>
		<Link onClick={handleAddMenu} css={addMenuLink} to="/admin/rides/add">
			Ride
		</Link>
		<Link
			onClick={handleAddMenu}
			css={addMenuLink}
			to="/admin/rides/categories/add"
		>
			Ride Category
		</Link>
		<Link
			onClick={handleAddMenu}
			css={addMenuLink}
			to="/admin/rides/types/add"
		>
			Ride Type
		</Link>
		<Link
			onClick={handleAddMenu}
			css={addMenuLink}
			to="/admin/achievements/add"
		>
			Achievement
		</Link>
	</div>
);

const Header: React.FC = () => {
	const [addMenu, toggleAddMenu] = useState(false);
	const [sidebar, toggleSidebar] = useState(false);

	const handleAddMenu = () => {
		toggleAddMenu(!addMenu);
	};

	const handleSidebar = () => {
		toggleSidebar(!sidebar);
	};

	return (
		<div css={headerStyle}>
			<div css={headerWrapper}>
				<div
					css={sidebar ? iconContainerActive : iconContainer}
					onClick={handleSidebar}
					onKeyDown={handleSidebar}
					tabIndex={-1}
					role="button"
					aria-label="overlayClickable"
				>
					<img src={menuIcon} alt="menu" css={iconStyle} />
				</div>
				<div css={iconContainer}>
					<img src={homeIcon} alt="menu" css={iconStyle} />
				</div>
				<div
					css={addMenu ? iconContainerActive : iconContainer}
					onClick={handleAddMenu}
					onKeyDown={handleAddMenu}
					tabIndex={-1}
					role="button"
					aria-label="overlayClickable"
				>
					<img src={addIcon} alt="menu" css={iconStyle} />
				</div>
				<div css={iconContainerRight}>
					<img src={userIcon} alt="menu" css={iconStyle} />
				</div>
			</div>
			{addMenu ? (
				<div css={menuContainer}>
					<AddMenu handleAddMenu={handleAddMenu} />
				</div>
			) : null}
			{sidebar ? <Sidebar handleSidebar={handleSidebar} /> : null}
		</div>
	);
};

const headerStyle = css`
	height: 56px;
	background-color: black;
`;

const headerWrapper = css`
	height: 100%;
	padding-right: 16px;
	display: flex;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
`;

const iconContainer = css`
	display: flex;
	flex-direction: column;
	justify-content: center;
	width: 10%;
	height: 100%;
	padding-left: 12px;
	padding-right: 12px;
`;

const iconContainerActive = css`
	display: flex;
	flex-direction: column;
	justify-content: center;
	width: 10%;
	height: 100%;
	padding-left: 12px;
	padding-right: 12px;
	background-color: grey;
`;

const iconContainerRight = css`
	display: flex;
	flex-direction: column;
	justify-content: center;
	width: 10%;
	height: 100%;
	margin-left: 16px;
	align-self: flex-end;
	margin-left: auto;
`;

const iconStyle = css`
	height: 75%;
`;

const menuContainer = css`
	height: 100%;
	z-index: 10;
`;

const addMenuStyle = css`
	z-index: 10;
	display: flex;
	flex-direction: column;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
`;

const addMenuLink = css`
	width: 100%;
	background-color: grey;
	color: white;
	padding: 12px;
	text-decoration: none;
	z-index: 10;
`;
export default Header;
