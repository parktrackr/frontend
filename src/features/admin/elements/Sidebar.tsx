import React from 'react';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */

import { Link } from 'react-router-dom';

type TSidebar = {
	handleSidebar(): void;
};

const Sidebar: React.FC<TSidebar> = ({ handleSidebar }: any) => {
	return (
		<div css={sideBarBox}>
			<Link css={menuItem} onClick={handleSidebar} to="/admin">
				Dashboard
			</Link>
			<Link css={menuItem} onClick={handleSidebar} to="/admin/parks">
				Parks
			</Link>
			<Link css={menuItem} onClick={handleSidebar} to="/admin/rides">
				Rides
			</Link>
			<Link
				css={menuItem}
				onClick={handleSidebar}
				to="/admin/rides/categories"
			>
				Ride Categories
			</Link>
			<Link
				css={menuItem}
				onClick={handleSidebar}
				to="/admin/rides/types"
			>
				Ride Types
			</Link>
			<Link
				css={menuItem}
				onClick={handleSidebar}
				to="/admin/achievements"
			>
				Achievements
			</Link>
		</div>
	);
};

const sideBarBox = css`
	position: absolute;
	background-color: rgb(104, 104, 104);
	width: 200px;
	z-index: 10;
	height: 100vh;
	display: flex;
	flex-direction: column;
`;

const menuItem = css`
	height: 56px;
	display: flex;
	justify-content: center;
	align-items: center;
	font-family: Poppins;
	color: white;
	font-weight: 700;
	text-decoration: none;
	border-bottom: 2px white solid;
	background-color: rgb(50, 50, 50);
`;

export default Sidebar;
