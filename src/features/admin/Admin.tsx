import React, { useContext } from 'react';
import { Redirect, Switch, Route } from 'react-router-dom';
// Contexts
import { UserContext } from '../../contexts/UserContext';

import { Header } from './elements';
import {
	Parks,
	AddPark,
	EditPark,
	Rides,
	AddRide,
	EditRide,
	RideCategories,
	AddRideCategory,
	EditRideCategory,
	RideTypes,
	Achievements,
	AddAchievement,
	EditAchievement,
} from './features';

const Admin = () => {
	const { state: userState } = useContext(UserContext);
	const { loading, currentUser } = userState;

	if (loading) return <p>Loading..</p>;
	if (currentUser.me === null || currentUser.me.role !== 'admin') {
		return <Redirect to="/404" />;
	}

	return (
		<div>
			<Header />
			<Switch>
				<Route exact path="/admin/parks/" component={Parks} />
				<Route path="/admin/parks/add/" component={AddPark} />
				<Route path="/admin/parks/edit/:Id" component={EditPark} />
				<Route exact path="/admin/rides/" component={Rides} />
				<Route
					exact
					path="/admin/rides/categories"
					component={RideCategories}
				/>
				<Route
					path="/admin/rides/categories/add/"
					component={AddRideCategory}
				/>
				<Route
					path="/admin/rides/categories/edit/:Id"
					component={EditRideCategory}
				/>
				<Route exact path="/admin/rides/types" component={RideTypes} />
				<Route path="/admin/rides/add/" component={AddRide} />
				<Route path="/admin/rides/edit/:Id" component={EditRide} />
				<Route
					exact
					path="/admin/achievements"
					component={Achievements}
				/>
				<Route
					path="/admin/achievements/add"
					component={AddAchievement}
				/>
				<Route
					path="/admin/achievements/edit/:Id"
					component={EditAchievement}
				/>
			</Switch>
		</div>
	);
};

export default Admin;
