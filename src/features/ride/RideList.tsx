import React, { useContext, useEffect, useState } from 'react';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import { useQuery } from '@apollo/react-hooks';
import Select from 'react-select';
import { Helmet } from 'react-helmet';

// Contexts
// import { UserContext } from '../../contexts/UserContext';
import { HeaderContext } from '../../contexts/HeaderContext';

// TypeScript Types
import { TRide } from '../../types/type/Ride.types';

// Reducer Types
import { Types as headerTypes } from '../../reducers/headerReducer';

import {
	RIDE_CATEGORIES_QUERY,
	RIDES_QUERY,
} from '../../apollo/graphql/queries/ride';

const RideCount: React.FC = ({ history }: any) => {
	const [categoryFilter, setCategoryFilter] = useState<any>([]);
	const [textFilter, setTextFilter] = useState('');
	const [sort, setSort] = useState({
		label: 'Ascending',
		value: 'ASC',
	});
	// const { state: userState } = useContext(UserContext);
	const { dispatch: headerDispatch } = useContext(HeaderContext);
	// const { currentUser } = userState;

	const handleCategoryFilter = (id: any) => {
		if (categoryFilter.some((item: any) => item === id)) {
			const newState = categoryFilter.filter((item: any) => item !== id);
			setCategoryFilter(newState);
		} else if (categoryFilter.length > 0) {
			setCategoryFilter((state: any) => [...state, id]);
		} else {
			setCategoryFilter([id]);
		}
	};

	const {
		loading: ridesQueryLoading,
		error: ridesQueryError,
		data: ridesData,
	} = useQuery<any>(RIDES_QUERY, {
		onCompleted() {
			headerDispatch({
				type: headerTypes.LoadSubHeader,
				payload: {
					text: `Total rides in database: ${ridesData.rides.length}`,
				},
			});
		},
		onError() {
			console.error(ridesQueryError);
		},
	});

	const {
		loading: rideCategoriesQueryLoading,
		error: rideCategoriesQueryError,
		data: rideCategoriesData,
	} = useQuery<any>(RIDE_CATEGORIES_QUERY, {
		onError() {
			console.error(rideCategoriesQueryError);
		},
	});

	useEffect(() => {
		headerDispatch({
			type: headerTypes.LoadTitle,
			payload: {
				text: 'Rides',
			},
		});
		headerDispatch({
			type: headerTypes.LoadSubTitle,
			payload: {
				text: `All`,
			},
		});
		headerDispatch({
			type: headerTypes.LoadColor,
			payload: {
				text: '#FA857C',
			},
		});
		headerDispatch({
			type: headerTypes.LoadSubColor,
			payload: {
				text: '#E37870',
			},
		});
		headerDispatch({
			type: headerTypes.LoadTitleColor,
			payload: {
				text: '#FFFFFF',
			},
		});
		headerDispatch({
			type: headerTypes.LoadIcon,
			payload: {
				text:
					'https://parktrackr.ams3.digitaloceanspaces.com/assets/ride-categories/roller-coaster-white.svg',
			},
		});
	}, [headerDispatch]);

	if (rideCategoriesQueryLoading || ridesQueryLoading)
		return <p>Loading...</p>;

	const { rideCategories } = rideCategoriesData;
	const { rides } = ridesData;
	// const lengthGrouped = groupBySubKey(rideVisits, 'ride', 'length');
	// const heightGrouped = groupBySubKey(rideVisits, 'ride', 'height');
	// const highestRide = heightGrouped.sort(
	//	(a: any, b: any) => b.key - a.key
	// )[0];
	//	const groupedLength = lengthGrouped.map(
	//		(obj: any) => obj.key * obj.groupedData.data.length
	//	);
	// const totalLength = groupedLength.reduce((sum: any, x: any) => sum + x);
	// const { groupedData } = highestRide;
	// const { ride } = groupedData;
	// const { originalName } = ride;

	const filteredRidesOnCategory =
		categoryFilter.length > 0
			? rides.filter((item: any) =>
					categoryFilter.some(
						(it: any) => it === item.rideCategories[0].id
					)
			  )
			: rides;

	const filteredRidesOnText =
		textFilter !== ''
			? filteredRidesOnCategory.filter((data: any) =>
					data.originalName
						.toLowerCase()
						.includes(textFilter.toLowerCase())
			  )
			: filteredRidesOnCategory;

	const sortArray = (array: []) => {
		switch (sort.value) {
			case 'ASC': {
				return array.sort((a: any, b: any) =>
					a.originalName.localeCompare(b.originalName)
				);
			}
			case 'DESC': {
				return array
					.sort((a: any, b: any) =>
						a.originalName.localeCompare(b.originalName)
					)
					.reverse();
			}
			default: {
				return array;
			}
		}
	};

	const sortOptions = [
		{
			label: 'Ascending A-Z',
			value: 'ASC',
		},
		{
			label: 'Descending Z-A',
			value: 'DESC',
		},
	];

	const filteredAndSorted = sortArray(filteredRidesOnText);

	return (
		<div>
			<Helmet>
				<title>Ride List - ParkTrackr</title>
			</Helmet>
			<div css={inputFieldContainer}>
				<input
					type="text"
					placeholder="Search..."
					css={InputField}
					defaultValue={textFilter}
					onChange={e => {
						setTextFilter(e.target.value);
					}}
				/>
			</div>
			<div css={categoriesContainer}>
				{rideCategories.map((cat: any) => {
					return (
						<div key={cat.id} css={categoryButton}>
							<div
								css={categoryIconContainer}
								style={{
									backgroundColor: categoryFilter.some(
										(item: any) => item === cat.id
									)
										? cat.color
										: 'rgba(0,0,0,0.5)',
								}}
								role="button"
								onClick={() => handleCategoryFilter(cat.id)}
								onKeyDown={() => handleCategoryFilter(cat.id)}
								tabIndex={-1}
							>
								<object
									data={cat.icon.replace(
										'.svg',
										'-white.svg'
									)}
									type="image/svg+xml"
									css={categoryIconStyle}
									aria-label={cat.originalName}
								/>
								<div css={iconTriangleSmall} />
							</div>
						</div>
					);
				})}
			</div>
			<Select
				css={sortSelect}
				options={sortOptions}
				value={sort}
				onChange={(e: any) => {
					setSort(e);
				}}
			/>

			<div css={itemContainer}>
				{filteredAndSorted &&
					filteredAndSorted.map((item: any) => {
						return (
							<RideCountItem
								item={item}
								key={item.id}
								history={history}
							/>
						);
					})}
			</div>
		</div>
	);
};

type TRideCountItem = {
	item: TRide;
	history: any;
};

const RideCountItem: React.FC<TRideCountItem> = ({ item, history }) => {
	const { originalName, slug, park, tempImage } = item;
	const { slug: parkSlug } = park;
	return (
		<div
			css={rideContainer}
			role="button"
			onKeyDown={() => history.push(`/park/${parkSlug}/ride/${slug}`)}
			tabIndex={-1}
			onClick={() => history.push(`/park/${parkSlug}/ride/${slug}`)}
		>
			<div css={overlayStyle} />
			<img src={tempImage} alt="ride" css={imageStyle} />
			<p css={rideName}>{originalName}</p>
			<div css={iconTriangle} />
			<div css={counterContainer}>
				<p css={counterText}>0</p>
			</div>
		</div>
	);
};

const itemContainer = css`
	display: flex;
	padding: 10px 0;
	flex-wrap: wrap;
	flex-grow: 2;
`;

const rideContainer = css`
	position: relative;
	height: 175px;
	width: 175px;
	overflow: hidden;
	z-index: 1;
	border-radius: 4px;
	margin: 6px 6px;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
`;

const imageStyle = css`
	height: 100%;
	width: 100%;
	object-fit: cover;
`;

const rideName = css`
	position: absolute;
	color: white;
	z-index: 8;
	bottom: 8px;
	margin: 0;
	left: 8px;
	font-weight: 700;
`;

const iconTriangle = css`
	width: 0;
	height: 0;
	border-bottom: 20px solid rgba(255, 255, 255, 0.5);
	border-left: 20px solid transparent;
	position: absolute;
	bottom: 8px;
	right: 8px;
	z-index: 8;
`;
const overlayStyle = css`
	position: absolute;
	background-image: linear-gradient(
		rgba(0, 0, 0, 0),
		rgba(0, 0, 0, 0),
		rgba(0, 0, 0, 0),
		rgba(0, 0, 0, 0),
		rgba(0, 0, 0, 0.4)
	);
	width: 100%;
	height: 100%;
	z-index: 3;
`;

const counterContainer = css`
	position: absolute;
	top: 8px;
	right: 8px;
	background-color: rgba(255, 255, 255, 0.7);
	padding: 4px;
	border-radius: 4px;
`;

const counterText = css`
	margin: 0;
	font-weight: 700;
`;

const categoriesContainer = css`
	display: flex;
	width: 100%;
	height: 150px;
	flex-wrap: wrap;
	justify-content: center;
	border-bottom: 2px solid white;
	padding-bottom: 12px;
	padding-top: 12px;
`;

const categoryButton = css`
	position: relative;
	width: 80px;
	height: 72px;
	box-sizing: border-box;
`;

const categoryIconContainer = css`
	width: 72px;
	height: 72px;
	display: flex;
	justify-content: center;
	position: absolute;
	border-width: 5px;
	border-style: solid;
	border-color: #f5f1f1;
	align-items: center;
	border-radius: 8px;
	box-sizing: border-box;
`;
const categoryIconStyle = css`
	max-width: 60%;
	height: 60%;
	pointer-events: none;
	box-sizing: border-box;
`;

const iconTriangleSmall = css`
	width: 0;
	height: 0;
	border-bottom: 10px solid rgba(255, 255, 255, 0.5);
	border-left: 10px solid transparent;
	position: absolute;
	bottom: 8px;
	right: 8px;
	z-index: 8;
`;

const inputFieldContainer = css`
	width: 100%;
	height: 72px;
	display: flex;
	justify-content: center;
	padding: 12px;
	box-sizing: border-box;
	border-bottom: 2px solid white;
`;

const InputField = css`
	width: 80%;
	height: 48px;
	box-sizing: border-box;
	color: rgba(0, 0, 0, 0.5);
	font-size: 1.1em;
	font-weight: bold;
	padding: 8px;
	border: 0;
	border-radius: 3px;
	text-align: center;
	outline-width: 0;
`;

const sortSelect = css`
	z-index: 9;
`;

export default RideCount;
