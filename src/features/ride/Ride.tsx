import React, { useState, useContext } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import { Helmet } from 'react-helmet';

// Elements
import { Link } from 'react-router-dom';

// Contexts
import { UserContext } from '../../contexts/UserContext';
import { HeaderContext } from '../../contexts/HeaderContext';

// Reducer Types
import { Types as headerTypes } from '../../reducers/headerReducer';

// Queries
import { RIDE_QUERY_BY_SLUG } from '../../apollo/graphql/queries/ride';

// Mutations
import { ADD_RIDE_TO_COUNT } from '../../apollo/graphql/mutations/user';

// TypeScript Types
import { TMatch } from '../../types/type/React.types';

// Misc.
import sitDown from '../../assets/icons/sit-down.svg';

const intensityIcons = [
	'https://parktrackr.ams3.digitaloceanspaces.com/assets/intensity/intensity-0.svg',
	'https://parktrackr.ams3.digitaloceanspaces.com/assets/intensity/intensity-1.svg',
	'https://parktrackr.ams3.digitaloceanspaces.com/assets/intensity/intensity-2.svg',
	'https://parktrackr.ams3.digitaloceanspaces.com/assets/intensity/intensity-3.svg',
];

const intensityColors = ['#40C077', '#FFC200', '#FD9326', '#bc2526'];

type TRide = {
	match: TMatch;
};

const Ride: React.FC<TRide> = ({ match }) => {
	const [visited, toggleVisited] = useState<boolean>(false);
	const [amountOfVisits, setAmountOfVisits] = useState<number>(0);

	const { params } = match;
	const { parkId, rideId } = params;

	const { state: userState } = useContext(UserContext);
	const { currentUser } = userState;

	const { dispatch: headerDispatch } = useContext(HeaderContext);

	const {
		loading: rideQueryLoading,
		error: rideQueryError,
		data: rideData,
	} = useQuery<any>(RIDE_QUERY_BY_SLUG, {
		variables: { slug: rideId },
		onCompleted() {
			if (rideData.ride !== null) {
				headerDispatch({
					type: headerTypes.LoadTitle,
					payload: {
						text: rideData.ride.originalName,
					},
				});
				headerDispatch({
					type: headerTypes.LoadSubTitle,
					payload: {
						text: rideData.ride.park.originalName,
					},
				});
				headerDispatch({
					type: headerTypes.LoadColor,
					payload: {
						text: '#FFFFFF',
					},
				});
				headerDispatch({
					type: headerTypes.LoadSubColor,
					payload: {
						text: rideData.ride.rideCategories[0].color,
					},
				});
				headerDispatch({
					type: headerTypes.LoadIcon,
					payload: {
						text: rideData.ride.rideCategories[0].icon.replace(
							'.svg',
							'-white.svg'
						),
					},
				});
				headerDispatch({
					type: headerTypes.LoadTitleColor,
					payload: {
						text: '#000000',
					},
				});
				headerDispatch({
					type: headerTypes.LoadSubHeader,
					payload: {
						text: null,
					},
				});
			}
		},
		onError() {
			console.error(rideQueryError);
		},
	});

	if (
		rideData &&
		currentUser?.me.rideVisits.some(
			(e: any) => e.ride.id === rideData.ride.id
		) &&
		visited === false
	) {
		toggleVisited(true);
	}

	if (
		rideData &&
		currentUser?.me.rideVisits.some(
			(e: any) => e.ride.id === rideData.ride.id
		) &&
		visited === true &&
		amountOfVisits === 0
	) {
		const amountRides = currentUser?.me.rideVisits.filter(
			(e: any) => e.ride.id === rideData.ride.id
		);
		setAmountOfVisits(amountRides.length);
	}

	const [addRideToCount] = useMutation(ADD_RIDE_TO_COUNT, {
		variables: { rideId: rideData?.ride ? rideData?.ride.id : '' },
		onError(err) {
			console.error(err);
		},
	});

	const handleAddCount = () => {
		if (currentUser?.me) {
			addRideToCount();
		} else {
			console.log('You are not signed in');
		}
	};

	if (rideQueryLoading) return <p>Loading</p>;

	if (rideData?.ride === null) return <p>No ride found</p>;

	if (rideData?.ride.park.slug === parkId) {
		const { ride } = rideData;
		const {
			park,
			originalName,
			intensity,
			height,
			length,
			tempImage,
		} = ride;
		const { slug: parkSlug } = park;
		return (
			<div>
				<Helmet>
					<title>{originalName} - ParkTrackr</title>
				</Helmet>
				<div css={imageWrapper}>
					<img src={tempImage} css={imageStyle} alt="park" />
				</div>
				<div css={lineStyle}>
					<div
						css={iconContainerTwo}
						onClick={() => console.log('Position Clicked')}
						role="button"
						onKeyDown={() => console.log('Position Clicked')}
						tabIndex={-1}
					>
						<img
							src={sitDown}
							css={iconStyle}
							alt="intensity-level"
						/>
						<div css={iconTriangle} />
					</div>
					<div
						css={iconContainer}
						onClick={() => console.log('Intensity Clicked')}
						role="button"
						onKeyDown={() => console.log('Intensity Clicked')}
						tabIndex={-1}
						style={{
							backgroundColor: intensityColors[intensity],
						}}
					>
						<img
							src={intensityIcons[intensity].replace(
								'.svg',
								'-white.svg'
							)}
							css={iconStyle}
							alt="intensity-level"
						/>
						<div css={iconTriangle} />
					</div>
				</div>
				<div css={buttonsContainer}>
					{visited ? (
						<div>
							<button
								css={addToCountButton}
								type="button"
								onClick={handleAddCount}
							>
								<object
									data="https://parktrackr.ams3.digitaloceanspaces.com/assets/credits/credits.svg"
									type="image/svg+xml"
									css={categoryIconStyle}
									aria-label="category"
								/>
								<p css={infoTextButton}>{amountOfVisits}</p>
							</button>
						</div>
					) : (
						<button
							css={addToCountButton}
							type="button"
							onClick={handleAddCount}
						>
							<object
								data="https://parktrackr.ams3.digitaloceanspaces.com/assets/credits/credits-add.svg"
								type="image/svg+xml"
								css={categoryIconStyle}
								aria-label="category"
							/>
							<p css={infoTextButton}>0</p>
						</button>
					)}
					<Link
						to={`/park/${parkSlug}`}
						css={addToCountButton}
						type="button"
					>
						<div css={innerButton}>
							<object
								data="https://parktrackr.ams3.digitaloceanspaces.com/assets/park/park.svg"
								type="image/svg+xml"
								css={categoryIconStyle}
								aria-label="park"
							/>
							<p css={infoTextButton}>Park</p>
						</div>
					</Link>
				</div>
				<div>
					<div
						css={categoryHeader}
						style={{
							backgroundColor: '#716262',
						}}
					>
						<div
							css={categoryIconContainer}
							style={{
								backgroundColor: '#716262',
							}}
						>
							<object
								data="https://parktrackr.ams3.digitaloceanspaces.com/assets/stats/stats-white.svg"
								type="image/svg+xml"
								css={statsIconStyle}
								aria-label="statistics"
							/>
						</div>
						<p css={categoryTitle}>Statisctics</p>
					</div>
					<div css={itemContainer}>
						{height ? (
							<div css={statCellHeight}>
								<div css={valueLarge}>{height}m</div>
								<div css={propertySmallRight}>High</div>
							</div>
						) : null}
						{length ? (
							<div css={statCellHeight}>
								<div css={valueLarge}>{length}m</div>
								<div css={propertySmallLeft}>Length</div>
							</div>
						) : null}
					</div>
				</div>
			</div>
		);
	}
	return <p>No ride found</p>;
};

const imageWrapper = css`
	position: relative;
	height: 300px;
	width: 100%;
	overflow: hidden;
	z-index: 1;
`;

const imageStyle = css`
	height: 100%;
	width: 100%;
	object-fit: cover;
`;

const lineStyle = css`
	width: 100%;
	height: 6px;
	background-color: #ffffff;
	position: relative;
`;

const iconContainer = css`
	width: 70px;
	height: 60px;
	position: absolute;
	display: flex;
	align-items: center;
	justify-content: center;
	border-radius: 2px;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
	margin-top: -30px;
	z-index: 8;
	right: 3%;
`;

const iconContainerTwo = css`
	width: 70px;
	height: 60px;
	background-color: #2576bc;
	position: absolute;
	display: flex;
	align-items: center;
	justify-content: center;
	border-radius: 2px;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
	margin-top: -30px;
	z-index: 8;
	right: 24%;
`;

const iconTriangle = css`
	width: 0;
	height: 0;
	border-bottom: 10px solid rgba(0, 0, 0, 0.15);
	border-left: 10px solid transparent;
	position: absolute;
	bottom: 5px;
	right: 3px;
`;

const iconStyle = css`
	max-width: 60%;
`;

const categoryHeader = css`
	width: 100%;
	height: 12px;
	margin-top: 30px;
	position: relative;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
`;

const categoryIconContainer = css`
	width: 60px;
	height: 60px;
	display: flex;
	justify-content: center;
	border-radius: 5px;
	position: absolute;
	left: 12px;
	top: -16px;
	border-width: 5px;
	border-style: solid;
	border-color: #f5f1f1;
	align-items: center;
	border-radius: 8px;
`;
const categoryIconStyle = css`
	min-width: 70%;
	min-height: 70%;
	pointer-events: none;
`;

const statsIconStyle = css`
	min-width: 70%;
	min-height: 70%;
	pointer-events: none;
`;

const categoryTitle = css`
	font-family: Poppins;
	font-weight: 700;
	font-size: 1.2em;
	position: absolute;
	left: 86px;
	top: -4px;
`;

const itemContainer = css`
	margin-top: 48px;
	display: flex;
	flex-direction: row;
	justify-content: space-evenly;
	flex-wrap: wrap;
	padding: 12px;
	align-items: center;
`;

const statCellHeight = css`
	display: flex;
	flex-direction: column;
	justify-content: center;
	padding-bottom: 12px;
`;

const valueLarge = css`
	font-family: Poppins;
	font-weight: 700;
	font-size: 2em;
`;

const propertySmallRight = css`
	text-align: right;
	margin-top: -12px;
	color: grey;
`;

const propertySmallLeft = css`
	text-align: left;
	margin-top: -12px;
	color: grey;
`;

const addToCountButton = css`
	background-color: transparent;
	z-index: 8;
	width: 52px;
	border-radius: 4px;
	border: none;
	color: black;
	font-family: 'Poppins';
	font-weight: 700;
	font-size: 1.5em;
	position: relative;
	cursor: pointer;
	text-decoration: none !important;
`;

const buttonsContainer = css`
	margin-top: 24px;
	height: 64px;
	width: 100%;
	display: flex;
	flex-direction: row;
	justify-content: space-evenly;
	padding: 12px;
	border-bottom: 3px solid white;
	text-decoration: none !important;
	box-sizing: border-box;
`;

const innerButton = css`
	display: flex;
	align-items: center;
	justify-content: center;
	flex-direction: column;
	height: 64px;
	text-decoration: none !important;
`;

const infoTextButton = css`
	margin-top: -4px;
	margin-bottom: 0;
	text-decoration: none !important;
	font-weight: 500;
	font-size: 16px;
`;
export default Ride;
