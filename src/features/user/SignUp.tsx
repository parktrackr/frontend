import React, { useState, useContext, useEffect } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';

// Contexts
import { UserContext } from '../../contexts/UserContext';
import { HeaderContext } from '../../contexts/HeaderContext';

// Reducer Types
import { Types as userTypes } from '../../reducers/userReducer';
import { Types as headerTypes } from '../../reducers/headerReducer';

// Mutations
import { SIGN_UP } from '../../apollo/graphql/mutations/user';

// TypeScript Types
import { THistory } from '../../types/type/React.types';

// Assets
import userIcon from '../../assets/icons/user.svg';

type TSignUp = {
	history: THistory;
};

const SignUp: React.FC<TSignUp> = ({ history }) => {
	const [username, setUsername] = useState<string>('');
	const [password, setPassword] = useState<string>('');
	const [email, setEmail] = useState<string>('');

	const { dispatch: userDispatch } = useContext(UserContext);
	const { dispatch: headerDispatch } = useContext(HeaderContext);

	useEffect(() => {
		headerDispatch({
			type: headerTypes.LoadTitle,
			payload: {
				text: 'Create Account',
			},
		});
		headerDispatch({
			type: headerTypes.LoadSubTitle,
			payload: {
				text: 'ParkTrackr',
			},
		});
		headerDispatch({
			type: headerTypes.LoadColor,
			payload: {
				text: '#FA857C',
			},
		});
		headerDispatch({
			type: headerTypes.LoadSubColor,
			payload: {
				text: '#E37870',
			},
		});
		headerDispatch({
			type: headerTypes.LoadTitleColor,
			payload: {
				text: '#FFFFFF',
			},
		});
		headerDispatch({
			type: headerTypes.LoadIcon,
			payload: {
				text: userIcon,
			},
		});
		headerDispatch({
			type: headerTypes.LoadSubHeader,
			payload: {
				text: ' ',
			},
		});
	}, [headerDispatch]);

	const SignUserUp = async (token: any) => {
		localStorage.setItem('x-token', token);
		userDispatch({
			type: userTypes.Load,
			payload: {
				token,
			},
		});
		history.push('/dashboard');
	};

	const [signUp] = useMutation(SIGN_UP, {
		variables: { username, password, email, role: 'USER' },
		onCompleted({ createUser: signUpData }) {
			SignUserUp(signUpData.token);
		},
		onError(err) {
			console.error(err);
		},
	});

	return (
		<div css={pageWrapper}>
			<Helmet>
				<title>Create Account - ParkTrackr</title>
			</Helmet>
			<form css={formStyle}>
				<div>
					<p css={labelStyle}>Username:</p>
					<input
						css={inputField}
						type="text"
						id="login"
						onChange={e => setUsername(e.target.value)}
					/>
				</div>

				<div>
					<p css={labelStyle}>Email:</p>
					<input
						css={inputField}
						type="text"
						id="email"
						onChange={e => setEmail(e.target.value)}
					/>
				</div>
				<div>
					<p css={labelStyle}>Password:</p>
					<input
						css={inputField}
						type="password"
						id="password"
						onChange={(e): void => setPassword(e.target.value)}
					/>
				</div>
				<div>
					<button
						css={buttonStyle}
						type="submit"
						onClick={e => {
							e.preventDefault();
							signUp();
						}}
					>
						Create Account
					</button>
				</div>
				<div css={signInLink}>
					<Link css={buttonStyle} to="/signin">
						Sign in
					</Link>
				</div>
			</form>
		</div>
	);
};

const pageWrapper = css`
	padding: 12px;
`;

const formStyle = css`
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
`;

const labelStyle = css`
	margin-bottom: 12px;
`;

const inputField = css`
	width: 100%;
	height: 48px;
	box-sizing: border-box;
	color: rgba(0, 0, 0, 0.5);
	font-size: 1.1em;
	font-weight: bold;
	padding: 8px;
	border: 0;
	border-radius: 3px;
	text-align: left;
	outline-width: 0;
`;

const buttonStyle = css`
	margin-top: 48px;
	background-color: #fa857c;
	color: white;
	padding: 24px 48px;
	border: none;
	border-radius: 3px;
	position: relative;
	font-family: Poppins;
	font-weight: 700;
	font-size: 1.2em;
	text-decoration: none;
`;

const signInLink = css`
	margin-top: 48px;
`;

export default SignUp;
