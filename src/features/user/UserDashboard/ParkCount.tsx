import React, { useContext, useEffect } from 'react';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import { Helmet } from 'react-helmet';

// Contexts
import { UserContext } from '../../../contexts/UserContext';
import { HeaderContext } from '../../../contexts/HeaderContext';

// Reducer Types
import { Types as headerTypes } from '../../../reducers/headerReducer';

// TypeScript Types
import { TPark } from '../../../types/type/Park.types';

import { groupBySubKey } from '../../../utils/helper';

// Assets
import creditsIcon from '../../../assets/icons/credits.svg';

const ParkCount: React.FC = ({ history }: any) => {
	const { state } = useContext(UserContext);
	const { currentUser } = state;
	const { dispatch: headerDispatch } = useContext(HeaderContext);

	useEffect(() => {
		if (currentUser) {
			if (currentUser.me) {
				headerDispatch({
					type: headerTypes.LoadTitle,
					payload: {
						text: 'Park Count',
					},
				});
				headerDispatch({
					type: headerTypes.LoadSubTitle,
					payload: {
						text: `${currentUser.me.username}'s`,
					},
				});
				headerDispatch({
					type: headerTypes.LoadColor,
					payload: {
						text: '#FA857C',
					},
				});
				headerDispatch({
					type: headerTypes.LoadSubColor,
					payload: {
						text: '#E37870',
					},
				});
				headerDispatch({
					type: headerTypes.LoadTitleColor,
					payload: {
						text: '#FFFFFF',
					},
				});
				headerDispatch({
					type: headerTypes.LoadIcon,
					payload: {
						text: creditsIcon,
					},
				});
				headerDispatch({
					type: headerTypes.LoadSubHeader,
					payload: {
						text: `Unique Parks: ${
							groupBySubKey(
								currentUser.me.parkVisits,
								'park',
								'id'
							).length
						}`,
					},
				});
			}
		}
	}, [currentUser, headerDispatch]);
	if (currentUser && currentUser.me) {
		const { me } = currentUser;
		const { parkVisits } = me;
		if (parkVisits === null) {
			return (
				<div>
					<p>No parks in the count!</p>
				</div>
			);
		}

		const parkGrouped = groupBySubKey(parkVisits, 'park', 'id');
		return (
			<div>
				<Helmet>
					<title>Park Count - Dashboard - ParkTrackr</title>
				</Helmet>
				<div css={itemContainer}>
					{parkGrouped &&
						parkGrouped.map((item: any) => {
							return (
								<ParkCountItem
									item={item}
									key={item.key}
									history={history}
								/>
							);
						})}
				</div>
				<button type="button" onClick={() => history.goBack()}>
					Go back
				</button>
			</div>
		);
	}
	return <p>Loading...</p>;
};

type PropsTypeParkCountItem = {
	item: {
		key: string;
		groupedData: {
			park: TPark;
			data: [
				{
					id: string;
					visitDate: Date;
					park: TPark;
				}
			];
		};
	};
	history: any;
};

const ParkCountItem: React.FC<PropsTypeParkCountItem> = ({ item, history }) => {
	const { groupedData } = item;
	const { park, data } = groupedData;
	const { originalName, slug, tempImage } = park;

	return (
		<div
			css={rideContainer}
			role="button"
			onKeyDown={() => history.push(`/park/${slug}`)}
			tabIndex={-1}
			onClick={() => history.push(`/park/${slug}`)}
		>
			<div css={overlayStyle} />
			<img src={tempImage} alt="ride" css={imageStyle} />
			<p css={rideName}>{originalName}</p>
			<div css={iconTriangle} />
			<div css={counterContainer}>
				<p css={counterText}>{data.length}</p>
			</div>
		</div>
	);
};

const itemContainer = css`
	display: flex;
	padding: 10px 0;
	flex-wrap: wrap;
	flex-grow: 2;
`;

const rideContainer = css`
	position: relative;
	height: 175px;
	width: 175px;
	overflow: hidden;
	z-index: 1;
	border-radius: 4px;
	margin: 6px 6px;
`;

const imageStyle = css`
	height: 100%;
	width: 100%;
	object-fit: cover;
`;

const rideName = css`
	position: absolute;
	color: white;
	z-index: 8;
	bottom: 8px;
	margin: 0;
	left: 8px;
	font-weight: 700;
`;

const iconTriangle = css`
	width: 0;
	height: 0;
	border-bottom: 20px solid rgba(255, 255, 255, 0.5);
	border-left: 20px solid transparent;
	position: absolute;
	bottom: 8px;
	right: 8px;
	z-index: 8;
`;
const overlayStyle = css`
	position: absolute;
	background-image: linear-gradient(
		rgba(0, 0, 0, 0),
		rgba(0, 0, 0, 0),
		rgba(0, 0, 0, 0),
		rgba(0, 0, 0, 0),
		rgba(0, 0, 0, 0.4)
	);
	width: 100%;
	height: 100%;
	z-index: 3;
`;

const counterContainer = css`
	position: absolute;
	top: 8px;
	right: 8px;
	background-color: rgba(255, 255, 255, 0.5);
	padding: 4px;
	border-radius: 4px;
`;

const counterText = css`
	margin: 0;
`;

export default ParkCount;
