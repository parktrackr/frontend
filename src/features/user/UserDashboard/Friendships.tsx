import React, { useContext } from 'react';
import moment from 'moment';
import { Helmet } from 'react-helmet';

// Contexts
import { UserContext } from '../../../contexts/UserContext';

const Friendships = ({ history }: any) => {
	const { state: userState } = useContext(UserContext);
	const { currentUser } = userState;

	if (!currentUser) return <p>Loading...</p>;
	const { me } = currentUser;
	const { friendships } = me;

	return (
		<div>
			<Helmet>
				<title>Friendships - Dashboard - ParkTrackr</title>
			</Helmet>
			<p>Friend Requests</p>
			{friendships ? (
				friendships.map((req: any) => {
					return (
						<div key={req.id}>
							<p>
								{req.requester.username === me.username
									? req.addressee.username
									: req.requester.username}
							</p>
							<p>
								{moment(req.registerDate).format(
									'MMMM Do YYYY, HH:mm'
								)}
							</p>
						</div>
					);
				})
			) : (
				<p>No Friendships</p>
			)}
			<button type="button" onClick={() => history.goBack()}>
				Go back
			</button>
		</div>
	);
};

export default Friendships;
