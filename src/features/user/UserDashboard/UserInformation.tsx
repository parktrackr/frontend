import React, { useContext } from 'react';
import moment from 'moment';
import { Helmet } from 'react-helmet';

// Contexts
import { UserContext } from '../../../contexts/UserContext';

// Helper Functions
import { groupBySubKey } from '../../../utils/helper';

const UserInformation = ({ history }: any) => {
	const { state: userState } = useContext(UserContext);
	const { currentUser } = userState;
	if (currentUser !== null) {
		const { me } = currentUser;

		const parkGrouped = groupBySubKey(me.parkVisits, 'park', 'id');
		const rideGrouped = groupBySubKey(me.rideVisits, 'ride', 'id');

		return (
			<div>
				<Helmet>
					<title>User Information - Dashboard - ParkTrackr</title>
				</Helmet>
				<p>User Information</p>
				<p>Username: {me.username}</p>
				<p>
					Register Date:{' '}
					{moment(me.registerDate).format('MMMM Do YYYY, HH:mm')}
				</p>
				<p>Email: {me.email}</p>
				<p>ParkVisits: {me.parkVisits.length}</p>
				<p>RideVisits: {me.rideVisits.length}</p>
				<p>Unique ParkVisits: {parkGrouped.length}</p>
				<p>Unique RideVisits: {rideGrouped.length}</p>
				<button type="button" onClick={() => history.goBack()}>
					Go back
				</button>
			</div>
		);
	}
	return <p>Loading...</p>;
};

export default UserInformation;
