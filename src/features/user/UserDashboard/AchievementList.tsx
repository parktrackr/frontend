import React, { useContext, useEffect } from 'react';
import {
	FacebookIcon,
	FacebookShareButton,
	TwitterIcon,
	TwitterShareButton,
} from 'react-share';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import { Helmet } from 'react-helmet';

// Contexts
import { UserContext } from '../../../contexts/UserContext';
import { HeaderContext } from '../../../contexts/HeaderContext';

// Reducer Types
import { Types as headerTypes } from '../../../reducers/headerReducer';

// Assets
import achievementIcon from '../../../assets/icons/achievement.svg';

const ParkCount: React.FC = ({ history }: any) => {
	const { state: userState } = useContext(UserContext);
	const { currentUser } = userState;
	const { dispatch: headerDispatch } = useContext(HeaderContext);

	useEffect(() => {
		if (currentUser) {
			if (currentUser.me) {
				headerDispatch({
					type: headerTypes.LoadTitle,
					payload: {
						text: 'Achievements',
					},
				});
				headerDispatch({
					type: headerTypes.LoadSubTitle,
					payload: {
						text: `${currentUser.me.username}'s`,
					},
				});
				headerDispatch({
					type: headerTypes.LoadColor,
					payload: {
						text: '#FA857C',
					},
				});
				headerDispatch({
					type: headerTypes.LoadSubColor,
					payload: {
						text: '#E37870',
					},
				});
				headerDispatch({
					type: headerTypes.LoadTitleColor,
					payload: {
						text: '#FFFFFF',
					},
				});
				headerDispatch({
					type: headerTypes.LoadIcon,
					payload: {
						text: achievementIcon,
					},
				});
				headerDispatch({
					type: headerTypes.LoadSubHeader,
					payload: {
						text: ' ',
					},
				});
			}
		}
	}, [currentUser, headerDispatch]);

	if (currentUser && currentUser.me) {
		const { me } = currentUser;
		const { achievementProgress } = me;
		return (
			<div>
				<div css={itemContainer}>
					{achievementProgress &&
						achievementProgress.map((item: any) => {
							return (
								<AchievementItem item={item} key={item.id} />
							);
						})}
				</div>
				<button type="button" onClick={() => history.goBack()}>
					Go back
				</button>
			</div>
		);
	}
	return <p>Loading...</p>;
};

type TAchievementItem = {
	item: any;
};

const AchievementItem: React.FC<TAchievementItem> = ({ item }) => {
	const { achievement, isCompleted, completedConditions } = item;
	const { translation, conditions, color, icon } = achievement;
	const { name } = translation;

	return (
		<div css={rideContainer} style={{ backgroundColor: color }}>
			<Helmet>
				<title>Achievements - Dashboard - ParkTrackr</title>
			</Helmet>
			<object
				data={icon}
				type="image/svg+xml"
				css={categoryIconStyle}
				aria-label={name}
			/>
			<p css={rideName}>{name}</p>
			<div css={iconTriangle} />
			<div css={counterContainer}>
				<p css={counterText}>
					{completedConditions.length} / {conditions.length}
				</p>
			</div>
			{isCompleted === true ? (
				<div>
					<FacebookShareButton
						url="http://parktrackr.com"
						quote={`I just got the achievement: ${name}, on ParkTrackr!`}
					>
						<FacebookIcon size={32} />{' '}
					</FacebookShareButton>
					<TwitterShareButton
						url="http://parktrackr.com"
						title={`I just got the achievement: ${name}, on ParkTrackr!`}
					>
						<TwitterIcon size={32} />{' '}
					</TwitterShareButton>
				</div>
			) : null}
		</div>
	);
};

export default ParkCount;

const itemContainer = css`
	display: flex;
	padding: 10px 0;
	flex-wrap: wrap;
	flex-grow: 2;
`;

const rideContainer = css`
	position: relative;
	height: 175px;
	width: 175px;
	overflow: hidden;
	z-index: 1;
	border-radius: 4px;
	margin: 6px 6px;
	display: flex;
	justify-content: center;
	align-items: center;
`;

const rideName = css`
	position: absolute;
	color: white;
	z-index: 8;
	bottom: 8px;
	margin: 0;
	left: 8px;
	font-weight: 700;
`;

const iconTriangle = css`
	width: 0;
	height: 0;
	border-bottom: 20px solid rgba(255, 255, 255, 0.5);
	border-left: 20px solid transparent;
	position: absolute;
	bottom: 8px;
	right: 8px;
	z-index: 8;
`;

const counterContainer = css`
	position: absolute;
	top: 8px;
	right: 8px;
	background-color: rgba(255, 255, 255, 0.5);
	padding: 4px;
	border-radius: 4px;
`;

const counterText = css`
	margin: 0;
`;

const categoryIconStyle = css`
	max-width: 60%;
	height: 60%;
	pointer-events: none;
	box-sizing: border-box;
	margin-top: -24px;
`;
