import React, { useContext } from 'react';
import { useMutation } from '@apollo/react-hooks';
import moment from 'moment';
import { Helmet } from 'react-helmet';

// Contexts
import { UserContext } from '../../../contexts/UserContext';

// Mutations
import { ACCEPT_FRIENDSHIP } from '../../../apollo/graphql/mutations/friendship';

const FriendRequests = ({ history }: any) => {
	const { state: userState } = useContext(UserContext);
	const { currentUser } = userState;
	const [acceptFriendship] = useMutation(ACCEPT_FRIENDSHIP, {
		onError(err) {
			console.error(err);
		},
	});

	if (!currentUser) return <p>Loading...</p>;
	const { me } = currentUser;
	const { friendshipRequests } = me;

	return (
		<div>
			<Helmet>
				<title>Friend Requests - Dashboard - ParkTrackr</title>
			</Helmet>
			<p>Friend Requests</p>
			{friendshipRequests.length > 0 ? (
				friendshipRequests.map((req: any) => {
					return (
						<div key={req.id}>
							<p>{req.requester.username}</p>
							<p>
								{moment(req.registerDate).format(
									'MMMM Do YYYY, HH:mm'
								)}
							</p>
							<button
								type="button"
								onClick={() =>
									acceptFriendship({
										variables: { friendshipId: req.id },
									})
								}
							>
								Accept Request
							</button>
						</div>
					);
				})
			) : (
				<p>No Friendship Requests</p>
			)}
			<button type="button" onClick={() => history.goBack()}>
				Go back
			</button>
		</div>
	);
};

export default FriendRequests;
