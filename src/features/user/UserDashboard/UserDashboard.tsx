import React, { useContext, useEffect } from 'react';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import { Helmet } from 'react-helmet';

// Elements
import { Link } from '../../../elements';

// Contexts
import { UserContext } from '../../../contexts/UserContext';
import { HeaderContext } from '../../../contexts/HeaderContext';

// Reducer Types
import { Types as headerTypes } from '../../../reducers/headerReducer';

import userIcon from '../../../assets/icons/user.svg';

const links = [
	{
		path: '/',
		text: 'Home',
	},
	{
		path: '/dashboard/parkcount',
		text: 'Park Count',
	},
	{
		path: '/dashboard/ridecount',
		text: 'Ride Count',
	},
	{
		path: '/dashboard/user',
		text: 'User Information',
	},
	{
		path: '/dashboard/friendrequests',
		text: 'Friendship Requests',
	},
	{
		path: '/dashboard/friendships',
		text: 'Friendships',
	},
	{
		path: '/dashboard/achievements',
		text: 'Achievements',
	},
];

const UserDashboard: React.FC = () => {
	const { state: userState } = useContext(UserContext);
	const { dispatch: headerDispatch } = useContext(HeaderContext);
	const { currentUser } = userState;
	useEffect(() => {
		if (currentUser) {
			if (currentUser.me) {
				headerDispatch({
					type: headerTypes.LoadTitle,
					payload: {
						text: 'Dashboard',
					},
				});
				headerDispatch({
					type: headerTypes.LoadSubTitle,
					payload: {
						text: `${currentUser.me.username}'s`,
					},
				});
				headerDispatch({
					type: headerTypes.LoadColor,
					payload: {
						text: '#FA857C',
					},
				});
				headerDispatch({
					type: headerTypes.LoadSubColor,
					payload: {
						text: '#E37870',
					},
				});
				headerDispatch({
					type: headerTypes.LoadTitleColor,
					payload: {
						text: '#FFFFFF',
					},
				});
				headerDispatch({
					type: headerTypes.LoadIcon,
					payload: {
						text: userIcon,
					},
				});
				headerDispatch({
					type: headerTypes.LoadSubHeader,
					payload: {
						text: 'a user dashboard',
					},
				});
			}
		}
	}, [currentUser, headerDispatch]);
	if (currentUser) {
		if (currentUser.me === null) {
			return <p>Loading...</p>;
		}

		return (
			<div>
				<Helmet>
					<title>Dashboard - ParkTrackr</title>
				</Helmet>
				<div css={dashboardContainer}>
					{links.map(link => (
						// eslint-disable-next-line jsx-a11y/anchor-is-valid
						<Link
							path={link.path}
							text={link.text}
							key={link.path}
						/>
					))}
					<button
						type="submit"
						onClick={e => {
							e.preventDefault();
							localStorage.removeItem('x-token');
							sessionStorage.clear();
						}}
					>
						Log out
					</button>
				</div>
			</div>
		);
	}
	return <p>Loading..</p>;
};

const dashboardContainer = css`
	display: flex;
	flex-direction: column;
	padding: 12px;
	height: 300px;
	justify-content: space-around;
`;

export default UserDashboard;
