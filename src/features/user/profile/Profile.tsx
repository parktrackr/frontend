import React, { useContext } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import moment from 'moment';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import { Helmet } from 'react-helmet';

import { Link } from 'react-router-dom';

// Contexts
import { HeaderContext } from '../../../contexts/HeaderContext';

// Reducer Types
import { Types as headerTypes } from '../../../reducers/headerReducer';

// Queries
import { GET_USER_BY_ID } from '../../../apollo/graphql/queries/user';

// Mutations
import { CREATE_FRIENDSHIP } from '../../../apollo/graphql/mutations/friendship';

// TypeScript Types
import { TMatch } from '../../../types/type/React.types';

import userIcon from '../../../assets/icons/user.svg';
import parkIcon from '../../../assets/icons/park.svg';
import rideIcon from '../../../assets/icons/ride.svg';
import achievementIcon from '../../../assets/icons/achievement.svg';

type TProfile = {
	match: TMatch;
};

const Profile: React.FC<TProfile> = ({ match }) => {
	const { params } = match;
	const { userId } = params;
	const { dispatch: headerDispatch } = useContext(HeaderContext);

	const {
		loading: userQueryLoading,
		error: userQueryError,
		data: userData,
	} = useQuery<any>(GET_USER_BY_ID, {
		variables: { username: userId },
		onCompleted() {
			headerDispatch({
				type: headerTypes.LoadTitle,
				payload: {
					text: userData.user.username,
				},
			});
			headerDispatch({
				type: headerTypes.LoadSubTitle,
				payload: {
					text: 'USER',
				},
			});
			headerDispatch({
				type: headerTypes.LoadColor,
				payload: {
					text: '#FA857C',
				},
			});
			headerDispatch({
				type: headerTypes.LoadSubColor,
				payload: {
					text: '#E37870',
				},
			});
			headerDispatch({
				type: headerTypes.LoadTitleColor,
				payload: {
					text: '#FFFFFF',
				},
			});
			headerDispatch({
				type: headerTypes.LoadIcon,
				payload: {
					text: userIcon,
				},
			});
			headerDispatch({
				type: headerTypes.LoadSubHeader,
				payload: {
					text: `Member since: ${moment(
						userData.user.registerDate
					).format('MMMM Do YYYY')}`,
				},
			});
		},
		onError() {
			console.error(userQueryError);
		},
	});

	const [createFriendship] = useMutation(CREATE_FRIENDSHIP, {
		variables: { userId: userData?.user ? userData?.user.id : '' },
		onError(err) {
			console.error(err);
		},
	});

	if (userQueryLoading && !userData) return <p>Loading...</p>;

	const groupBy = (array: [], key: any, subkey: any) => {
		return array.reduce((result: any, currentValue: any) => {
			if (!result[currentValue[key][subkey]]) {
				// eslint-disable-next-line no-param-reassign
				result[currentValue[key][subkey]] = {
					visits: [],
					[key]: currentValue[key],
				};
			}
			// eslint-disable-next-line no-param-reassign
			result[currentValue[key][subkey]] = {
				...result[currentValue[key][subkey]],
				visits: [
					...result[currentValue[key][subkey]].visits,
					currentValue,
				],
			};
			return result;
		}, []);
	};

	const parkGrouped = groupBy(userData.user.parkVisits, 'park', 'id');
	const rideGrouped = groupBy(userData.user.rideVisits, 'ride', 'id');

	const parkKeys = Object.keys(parkGrouped).map(key => {
		return {
			key,
			data: parkGrouped[key],
		};
	});

	const rideKeys = Object.keys(rideGrouped).map(key => {
		return {
			key,
			data: parkGrouped[key],
		};
	});

	return (
		<div css={pageContainer}>
			<Helmet>
				<title>{userData.user.username} - ParkTrackr</title>
			</Helmet>
			<Link to={`/user/${userId}/parkcount`} css={parkContainer}>
				<div css={iconContainer}>
					<img css={iconStyle} src={parkIcon} alt="page-emblem" />
				</div>
				<p css={rideName}>Park Count</p>
				<div css={iconTriangle} />
				<div css={counterContainer}>
					<p css={counterText}>{parkKeys.length}</p>
				</div>
			</Link>
			<Link to={`/user/${userId}/ridecount`} css={rideContainer}>
				<div css={iconContainer}>
					<img css={iconStyle} src={rideIcon} alt="page-emblem" />
				</div>
				<p css={rideName}>Ride Count</p>
				<div css={iconTriangle} />
				<div css={counterContainer}>
					<p css={counterText}>{rideKeys.length}</p>
				</div>
			</Link>
			<Link
				to={`/user/${userId}/achievements`}
				css={achievementsContainer}
			>
				<div css={iconContainer}>
					<img
						css={iconStyle}
						src={achievementIcon}
						alt="page-emblem"
					/>
				</div>
				<p css={rideName}>Achievements</p>
				<div css={iconTriangle} />
			</Link>
			<div css={buttonContainer}>
				<button
					css={buttonStyle}
					type="button"
					onClick={() => createFriendship()}
				>
					Send Friend Request
					<div css={iconTriangle} />
				</button>
			</div>
		</div>
	);
};

const pageContainer = css`
	padding: 12px 12px;
	display: flex;
	flex-direction: row;
	flex-wrap: wrap;
`;

const parkContainer = css`
	position: relative;
	height: 175px;
	width: 48%;
	overflow: hidden;
	z-index: 1;
	border-radius: 4px;
	margin-right: 12px;
	background-color: #40c077;
`;

const rideContainer = css`
	position: relative;
	height: 175px;
	width: 48%;
	overflow: hidden;
	z-index: 1;
	border-radius: 4px;
	background-color: #d43f36;
`;

const achievementsContainer = css`
	position: relative;
	height: 175px;
	width: 48%;
	overflow: hidden;
	z-index: 1;
	border-radius: 4px;
	background-color: #40c077;
	margin-top: 16px;
`;

const rideName = css`
	position: absolute;
	color: white;
	z-index: 8;
	bottom: 8px;
	margin: 0;
	left: 8px;
	font-weight: 700;
`;

const iconTriangle = css`
	width: 0;
	height: 0;
	border-bottom: 20px solid rgba(255, 255, 255, 0.5);
	border-left: 20px solid transparent;
	position: absolute;
	bottom: 8px;
	right: 8px;
	z-index: 8;
`;

const iconContainer = css`
	width: 100%;
	height: 100%;
	border-radius: 3px;
	position: relative;
	display: flex;
	align-items: center;
	justify-content: center;
	z-index: 10;
`;

const iconStyle = css`
	max-width: 70%;
`;

const counterContainer = css`
	position: absolute;
	top: 8px;
	right: 8px;
	background-color: rgba(255, 255, 255, 0.5);
	padding: 4px;
	border-radius: 4px;
`;

const counterText = css`
	margin: 0;
`;

const buttonContainer = css`
	width: 100%;
	display: flex;
	justify-content: center;
	padding: 24px;
`;

const buttonStyle = css`
	background-color: #fa857c;
	color: white;
	padding: 24px;
	border: none;
	border-radius: 3px;
	position: relative;
	font-family: Poppins;
	font-weight: 700;
	font-size: 1.2em;
`;
export default Profile;
