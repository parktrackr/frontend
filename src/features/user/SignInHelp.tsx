import React, { useContext, useEffect } from 'react';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import { Helmet } from 'react-helmet';

// Contexts
import { HeaderContext } from '../../contexts/HeaderContext';

// Reducer Types
import { Types as headerTypes } from '../../reducers/headerReducer';

import userIcon from '../../assets/icons/user.svg';

const SignInHelp: React.FC = ({ history }: any) => {
	const { dispatch: headerDispatch } = useContext(HeaderContext);

	useEffect(() => {
		headerDispatch({
			type: headerTypes.LoadTitle,
			payload: {
				text: 'Sign In',
			},
		});
		headerDispatch({
			type: headerTypes.LoadSubTitle,
			payload: {
				text: 'ParkTrackr',
			},
		});
		headerDispatch({
			type: headerTypes.LoadColor,
			payload: {
				text: '#FA857C',
			},
		});
		headerDispatch({
			type: headerTypes.LoadSubColor,
			payload: {
				text: '#E37870',
			},
		});
		headerDispatch({
			type: headerTypes.LoadTitleColor,
			payload: {
				text: '#FFFFFF',
			},
		});
		headerDispatch({
			type: headerTypes.LoadIcon,
			payload: {
				text: userIcon,
			},
		});
		headerDispatch({
			type: headerTypes.LoadSubHeader,
			payload: {
				text: 'Trouble Shooting',
			},
		});
	}, [headerDispatch]);
	return (
		<div css={pageWrapper}>
			<Helmet>
				<title>Trouble Shooting - Sign In - ParkTrackr</title>
			</Helmet>
			<p>Work in Progress</p>
			<button type="button" onClick={() => history.goBack()}>
				Go back
			</button>
		</div>
	);
};

const pageWrapper = css`
	padding: 12px;
`;

export default SignInHelp;
