import React, { useState, useContext, useEffect } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';

// Contexts
import { UserContext } from '../../contexts/UserContext';
import { HeaderContext } from '../../contexts/HeaderContext';

// Reducer Types
import { Types as userTypes } from '../../reducers/userReducer';
import { Types as headerTypes } from '../../reducers/headerReducer';

// Mutations
import { SIGN_IN } from '../../apollo/graphql/mutations/user';

// TypeScript Types
import { SignInMutationProps } from '../../types/type/User.types';
import { THistory } from '../../types/type/React.types';

import userIcon from '../../assets/icons/user.svg';

type TSignIn = {
	history: THistory;
};

const SignIn: React.FC<TSignIn> = ({ history }) => {
	const [login, setLogin] = useState<string>('');
	const [password, setPassword] = useState<string>('');

	const { dispatch: userDispatch } = useContext(UserContext);
	const { dispatch: headerDispatch } = useContext(HeaderContext);

	useEffect(() => {
		headerDispatch({
			type: headerTypes.LoadTitle,
			payload: {
				text: 'Sign In',
			},
		});
		headerDispatch({
			type: headerTypes.LoadSubTitle,
			payload: {
				text: 'ParkTrackr',
			},
		});
		headerDispatch({
			type: headerTypes.LoadColor,
			payload: {
				text: '#FA857C',
			},
		});
		headerDispatch({
			type: headerTypes.LoadSubColor,
			payload: {
				text: '#E37870',
			},
		});
		headerDispatch({
			type: headerTypes.LoadTitleColor,
			payload: {
				text: '#FFFFFF',
			},
		});
		headerDispatch({
			type: headerTypes.LoadIcon,
			payload: {
				text: userIcon,
			},
		});
		headerDispatch({
			type: headerTypes.LoadSubHeader,
			payload: {
				text: ' ',
			},
		});
	}, [headerDispatch]);

	const [signIn] = useMutation<SignInMutationProps>(SIGN_IN, {
		variables: { login, password },
		onCompleted({ signIn: data }) {
			localStorage.setItem('x-token', data.token);
			userDispatch({
				type: userTypes.Load,
				payload: {
					token: data.token,
				},
			});
			history.push('/dashboard');
		},
		onError(err) {
			console.error(err);
		},
	});

	return (
		<div css={pageWrapper}>
			<Helmet>
				<title>Sign In - ParkTrackr</title>
			</Helmet>
			<form css={formStyle}>
				<div>
					<p css={labelStyle}>Username or Email</p>
					<input
						css={inputField}
						type="text"
						id="login"
						onChange={e => setLogin(e.target.value)}
					/>
				</div>
				<div>
					<p css={labelStyle}>Password</p>
					<input
						css={inputField}
						type="password"
						id="password"
						onChange={(e): void => setPassword(e.target.value)}
					/>
				</div>
				<div>
					<button
						css={buttonStyle}
						type="submit"
						onClick={e => {
							e.preventDefault();
							signIn();
						}}
					>
						Log in
					</button>
				</div>
				<div css={signInLink}>
					<Link css={buttonStyle} to="/signup">
						Create Account
					</Link>
				</div>
				<div css={troubleLink}>
					<Link css={linkStyling} to="/signin/help">
						Trouble signing in?
					</Link>
				</div>
			</form>
		</div>
	);
};

const pageWrapper = css`
	padding: 12px;
`;

const formStyle = css`
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
`;

const labelStyle = css`
	margin-bottom: 12px;
`;

const inputField = css`
	width: 100%;
	height: 48px;
	box-sizing: border-box;
	color: rgba(0, 0, 0, 0.5);
	font-size: 1.1em;
	font-weight: bold;
	padding: 8px;
	border: 0;
	border-radius: 3px;
	text-align: left;
	outline-width: 0;
`;

const buttonStyle = css`
	margin-top: 48px;
	background-color: #fa857c;
	color: white;
	padding: 24px 48px;
	border: none;
	border-radius: 3px;
	position: relative;
	font-family: Poppins;
	font-weight: 700;
	font-size: 1.2em;
	text-decoration: none;
`;

const troubleLink = css`
	margin-top: 48px;
`;

const linkStyling = css`
	color: rgba(0, 0, 0, 0.6);
`;

const signInLink = css`
	margin-top: 48px;
`;

export default SignIn;
