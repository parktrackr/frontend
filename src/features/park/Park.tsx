import React, { useState, useContext, useEffect } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import { Helmet } from 'react-helmet';

// Contexts
import { UserContext } from '../../contexts/UserContext';
import { HeaderContext } from '../../contexts/HeaderContext';

// Reducer Types
import { Types as headerTypes } from '../../reducers/headerReducer';

// queries
import { PARK_QUERY_BY_SLUG } from '../../apollo/graphql/queries/park';

// mutations
import { ADD_PARK_TO_COUNT } from '../../apollo/graphql/mutations/user';

// Typescript Types
import { TMatch } from '../../types/type/React.types';
import { TAddParkToCountMutation } from '../../types/type/User.types';
// import { TRide } from '../../types/type/Ride.types';

// Misc.
import dutchFlag from '../../assets/flags/nl.svg';
import parkIcon from '../../assets/icons/park.svg';

import { groupBySubKey } from '../../utils/helper';

type TPark = {
	match: TMatch;
	history: any;
};

const Park: React.FC<TPark> = ({ match, history }) => {
	const [visited, toggleVisited] = useState<boolean>(false);

	const { params } = match;
	const { parkId } = params;

	const { state: userState } = useContext(UserContext);
	const { currentUser } = userState;

	const { dispatch: headerDispatch } = useContext(HeaderContext);

	useEffect(() => {
		headerDispatch({
			type: headerTypes.LoadSubHeader,
			payload: {
				text: null,
			},
		});
	}, [headerDispatch]);
	const {
		loading: parkQueryLoading,
		error: parkQueryError,
		data: parkData,
	} = useQuery<any>(PARK_QUERY_BY_SLUG, {
		variables: { slug: parkId },
		onCompleted() {
			if (parkData.park !== null) {
				headerDispatch({
					type: headerTypes.LoadTitle,
					payload: {
						text: parkData.park.originalName,
					},
				});
				headerDispatch({
					type: headerTypes.LoadSubTitle,
					payload: {
						text: 'LOCATION OF PARK',
					},
				});
				headerDispatch({
					type: headerTypes.LoadColor,
					payload: {
						text: '#FFFFFF',
					},
				});
				headerDispatch({
					type: headerTypes.LoadSubColor,
					payload: {
						text: '#40C077',
					},
				});
				headerDispatch({
					type: headerTypes.LoadTitleColor,
					payload: {
						text: '#000000',
					},
				});
				headerDispatch({
					type: headerTypes.LoadIcon,
					payload: {
						text: parkIcon,
					},
				});
			}
		},
		onError() {
			console.error(parkQueryError);
		},
	});

	if (
		parkData &&
		currentUser?.me.parkVisits.some(
			(e: any) => e.parkId === parkData.park.id
		) &&
		visited === false
	) {
		toggleVisited(true);
	}

	const [addParkToCount] = useMutation<TAddParkToCountMutation>(
		ADD_PARK_TO_COUNT,
		{
			variables: { parkId: parkData?.park ? parkData?.park.id : '' },
			onError(err) {
				console.error(err);
			},
		}
	);

	const handleAddCount = () => {
		if (currentUser?.me) {
			addParkToCount();
		} else {
			console.log('You are not signed in');
		}
	};

	if (parkQueryLoading) return <p>Loading</p>;

	if (parkData?.park === null) return <p>No park found</p>;
	const { park } = parkData;
	const { originalName, rides, tempImage } = park;

	const mappedRides = rides.map((ride: any) => {
		return {
			category: ride.rideCategories[0],
			...ride,
		};
	});

	const categoryGrouped = groupBySubKey(mappedRides, 'category', 'id');
	return (
		<div>
			<Helmet>
				<title>{originalName} - ParkTrackr</title>
			</Helmet>
			<div css={imageWrapper}>
				<img src={tempImage} css={imageStyle} alt="park" />
			</div>
			<div css={lineStyle}>
				<div
					css={flagContainer}
					onClick={() => console.log('Flag Clicked')}
					role="button"
					onKeyDown={() => console.log('Flag Clicked')}
					tabIndex={-1}
				>
					<img src={dutchFlag} css={flagStyle} alt="flag" />
					<div css={iconTriangleSmall} />
				</div>
			</div>
			<div>
				<div>
					{categoryGrouped &&
						categoryGrouped.map((group: any) => {
							const { groupedData } = group;
							const { data, category } = groupedData;
							const sortedData = data.sort((a: any, b: any) =>
								a.originalName > b.originalName ? 1 : -1
							);
							return (
								<div key={group.key}>
									<div
										css={categoryHeader}
										style={{
											backgroundColor: category.color,
										}}
									>
										<div
											css={categoryIconContainer}
											style={{
												backgroundColor: category.color,
											}}
										>
											<object
												data={category.icon.replace(
													'.svg',
													'-white.svg'
												)}
												type="image/svg+xml"
												css={categoryIconStyle}
												aria-label={
													category.originalName
												}
											/>
										</div>
										<p css={categoryTitle}>
											{category.originalName}
											{sortedData.length > 1 ? 's' : null}
										</p>
									</div>
									<div css={itemContainer}>
										{sortedData.map((item: any) => {
											return (
												<RideItem
													item={item}
													key={item.id}
													history={history}
													parkSlug={
														parkData.park.slug
													}
												/>
											);
										})}
									</div>
								</div>
							);
						})}
				</div>
			</div>
			{visited ? (
				<div>
					<p>This park is in your count!</p>
					<button type="button" onClick={handleAddCount}>
						Add to Count
					</button>
				</div>
			) : (
				<button type="button" onClick={handleAddCount}>
					Add to Count
				</button>
			)}
		</div>
	);
};

type TRideItem = {
	item: any;
	history: any;
	parkSlug: string;
};

const RideItem: React.FC<TRideItem> = ({ item, history, parkSlug }) => {
	const { originalName, slug, tempImage } = item;
	return (
		<div
			css={rideContainer}
			role="button"
			onKeyDown={() => history.push(`/park/${parkSlug}/ride/${slug}`)}
			tabIndex={-1}
			onClick={() => history.push(`/park/${parkSlug}/ride/${slug}`)}
		>
			<div css={overlayStyle} />
			<img src={tempImage} alt="ride" css={imageStyle} />
			<p css={rideName}>{originalName}</p>
			<div css={iconTriangle} />
		</div>
	);
};

const imageWrapper = css`
	position: relative;
	height: 300px;
	width: 100%;
	overflow: hidden;
	z-index: 1;
`;

const imageStyle = css`
	height: 100%;
	width: 100%;
	object-fit: cover;
`;

const lineStyle = css`
	width: 100%;
	height: 6px;
	background-color: #ffffff;
	position: relative;
`;

const flagContainer = css`
	width: 70px;
	height: 60px;
	background-color: white;
	position: absolute;
	display: flex;
	align-items: center;
	justify-content: center;
	border-radius: 2px;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
	margin-top: -30px;
	z-index: 8;
	right: 5%;
`;

const iconTriangleSmall = css`
	width: 0;
	height: 0;
	border-bottom: 10px solid rgba(0, 0, 0, 0.15);
	border-left: 10px solid transparent;
	position: absolute;
	bottom: 5px;
	right: 3px;
`;

const flagStyle = css`
	max-width: 70%;
`;

const itemContainer = css`
	margin-top: 48px;
	display: flex;
	flex-wrap: wrap;
	flex-grow: 2;
`;

const rideContainer = css`
	position: relative;
	height: 175px;
	width: 175px;
	overflow: hidden;
	z-index: 1;
	border-radius: 4px;
	margin: 6px 6px;
`;

const rideName = css`
	position: absolute;
	color: white;
	z-index: 8;
	bottom: 8px;
	margin: 0;
	left: 8px;
	font-weight: 700;
`;

const iconTriangle = css`
	width: 0;
	height: 0;
	border-bottom: 20px solid rgba(255, 255, 255, 0.5);
	border-left: 20px solid transparent;
	position: absolute;
	bottom: 8px;
	right: 8px;
	z-index: 8;
`;
const overlayStyle = css`
	position: absolute;
	background-image: linear-gradient(
		rgba(0, 0, 0, 0),
		rgba(0, 0, 0, 0),
		rgba(0, 0, 0, 0),
		rgba(0, 0, 0, 0),
		rgba(0, 0, 0, 0.4)
	);
	width: 100%;
	height: 100%;
	z-index: 3;
`;

const categoryHeader = css`
	width: 100%;
	height: 12px;
	margin-top: 30px;
	position: relative;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
`;

const categoryIconContainer = css`
	width: 60px;
	height: 60px;
	display: flex;
	justify-content: center;
	border-radius: 5px;
	position: absolute;
	left: 12px;
	top: -16px;
	border-width: 5px;
	border-style: solid;
	border-color: #f5f1f1;
	align-items: center;
	border-radius: 8px;
`;
const categoryIconStyle = css`
	max-width: 70%;
	height: 70%;
`;

const categoryTitle = css`
	font-family: Poppins;
	font-weight: 700;
	font-size: 1.2em;
	position: absolute;
	left: 86px;
	top: -4px;
`;

export default Park;
