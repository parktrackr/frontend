import React, { useContext, useEffect, useState } from 'react';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import { useQuery } from '@apollo/react-hooks';
import Select from 'react-select';

// Contexts
// import { UserContext } from '../../contexts/UserContext';
import { Helmet } from 'react-helmet';
import { HeaderContext } from '../../contexts/HeaderContext';

// TypeScript Types
import { TPark } from '../../types/type/Park.types';

// Reducer Types
import { Types as headerTypes } from '../../reducers/headerReducer';

import { PARKS_QUERY } from '../../apollo/graphql/queries/park';

const RideCount: React.FC = ({ history }: any) => {
	const [textFilter, setTextFilter] = useState('');
	const [sort, setSort] = useState({
		label: 'Ascending',
		value: 'ASC',
	});
	// const { state: userState } = useContext(UserContext);
	const { dispatch: headerDispatch } = useContext(HeaderContext);
	// const { currentUser } = userState;

	const {
		loading: parksQueryLoading,
		error: parksQueryError,
		data: parksData,
	} = useQuery<any>(PARKS_QUERY, {
		onCompleted() {
			headerDispatch({
				type: headerTypes.LoadSubHeader,
				payload: {
					text: `Total parks in database: ${parksData.parks.length}`,
				},
			});
		},
		onError() {
			console.error(parksQueryError);
		},
	});

	useEffect(() => {
		headerDispatch({
			type: headerTypes.LoadTitle,
			payload: {
				text: 'Parks',
			},
		});
		headerDispatch({
			type: headerTypes.LoadSubTitle,
			payload: {
				text: `All`,
			},
		});
		headerDispatch({
			type: headerTypes.LoadColor,
			payload: {
				text: '#FA857C',
			},
		});
		headerDispatch({
			type: headerTypes.LoadSubColor,
			payload: {
				text: '#E37870',
			},
		});
		headerDispatch({
			type: headerTypes.LoadTitleColor,
			payload: {
				text: '#FFFFFF',
			},
		});
		headerDispatch({
			type: headerTypes.LoadIcon,
			payload: {
				text:
					'https://parktrackr.ams3.digitaloceanspaces.com/assets/ride-categories/roller-coaster-white.svg',
			},
		});
	}, [headerDispatch]);

	if (parksQueryLoading) return <p>Loading...</p>;

	const { parks } = parksData;

	const filteredParksOnText =
		textFilter !== ''
			? parks.filter((data: any) =>
					data.originalName
						.toLowerCase()
						.includes(textFilter.toLowerCase())
			  )
			: parks;

	const sortArray = (array: []) => {
		switch (sort.value) {
			case 'ASC': {
				return array.sort((a: any, b: any) =>
					a.originalName.localeCompare(b.originalName)
				);
			}
			case 'DESC': {
				return array
					.sort((a: any, b: any) =>
						a.originalName.localeCompare(b.originalName)
					)
					.reverse();
			}
			default: {
				return array;
			}
		}
	};

	const sortOptions = [
		{
			label: 'Ascending A-Z',
			value: 'ASC',
		},
		{
			label: 'Descending Z-A',
			value: 'DESC',
		},
	];

	const filteredAndSorted = sortArray(filteredParksOnText);

	return (
		<div>
			<Helmet>
				<title>Park List - ParkTrackr</title>
			</Helmet>
			<div css={inputFieldContainer}>
				<input
					type="text"
					placeholder="Search..."
					css={InputField}
					defaultValue={textFilter}
					onChange={e => {
						setTextFilter(e.target.value);
					}}
				/>
			</div>
			<Select
				css={sortSelect}
				options={sortOptions}
				value={sort}
				onChange={(e: any) => {
					setSort(e);
				}}
			/>

			<div css={itemContainer}>
				{filteredAndSorted &&
					filteredAndSorted.map((item: any) => {
						return (
							<RideCountItem
								item={item}
								key={item.id}
								history={history}
							/>
						);
					})}
			</div>
		</div>
	);
};

type TRideCountItem = {
	item: TPark;
	history: any;
};

const RideCountItem: React.FC<TRideCountItem> = ({ item, history }) => {
	const { originalName, slug, tempImage } = item;
	return (
		<div
			css={rideContainer}
			role="button"
			onKeyDown={() => history.push(`/park/${slug}`)}
			tabIndex={-1}
			onClick={() => history.push(`/park/${slug}`)}
		>
			<div css={overlayStyle} />
			<img src={tempImage} alt="ride" css={imageStyle} />
			<p css={rideName}>{originalName}</p>
			<div css={iconTriangle} />
			<div css={counterContainer}>
				<p css={counterText}>0</p>
			</div>
		</div>
	);
};

const itemContainer = css`
	display: flex;
	padding: 10px 0;
	flex-wrap: wrap;
	flex-grow: 2;
`;

const rideContainer = css`
	position: relative;
	height: 175px;
	width: 175px;
	overflow: hidden;
	z-index: 1;
	border-radius: 4px;
	margin: 6px 6px;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
`;

const imageStyle = css`
	height: 100%;
	width: 100%;
	object-fit: cover;
`;

const rideName = css`
	position: absolute;
	color: white;
	z-index: 8;
	bottom: 8px;
	margin: 0;
	left: 8px;
	font-weight: 700;
`;

const iconTriangle = css`
	width: 0;
	height: 0;
	border-bottom: 20px solid rgba(255, 255, 255, 0.5);
	border-left: 20px solid transparent;
	position: absolute;
	bottom: 8px;
	right: 8px;
	z-index: 8;
`;
const overlayStyle = css`
	position: absolute;
	background-image: linear-gradient(
		rgba(0, 0, 0, 0),
		rgba(0, 0, 0, 0),
		rgba(0, 0, 0, 0),
		rgba(0, 0, 0, 0),
		rgba(0, 0, 0, 0.4)
	);
	width: 100%;
	height: 100%;
	z-index: 3;
`;

const counterContainer = css`
	position: absolute;
	top: 8px;
	right: 8px;
	background-color: rgba(255, 255, 255, 0.7);
	padding: 4px;
	border-radius: 4px;
`;

const counterText = css`
	margin: 0;
	font-weight: 700;
`;

const inputFieldContainer = css`
	width: 100%;
	height: 72px;
	display: flex;
	justify-content: center;
	padding: 12px;
	box-sizing: border-box;
	border-bottom: 2px solid white;
`;

const InputField = css`
	width: 80%;
	height: 48px;
	box-sizing: border-box;
	color: rgba(0, 0, 0, 0.5);
	font-size: 1.1em;
	font-weight: bold;
	padding: 8px;
	border: 0;
	border-radius: 3px;
	text-align: center;
	outline-width: 0;
`;

const sortSelect = css`
	z-index: 9;
`;

export default RideCount;
