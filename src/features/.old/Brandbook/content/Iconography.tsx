import React from 'react';
/* eslint-disable max-len */
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import iconthick from '../../../../assets/brand/brandbook/iconsthick.png';
import iconsthin from '../../../../assets/brand/brandbook/iconsthin.png';

const intro = css`
	font-family: 'Poppins', sans-serif;
`;

const logoStyle = css`
	width: 700px;
`;

const Iconography: React.FC = () => (
	<div>
		<h3 css={intro}>
			Icons who will be used in ParkTrackr will always follow a set grid.
		</h3>
		<h3 css={intro} style={{ marginTop: '50px' }}>
			ICONS
		</h3>
		<p>
			The height/width of an icon is based upon the grid of the trademark,
			to make sure there is consistency in the visual identity.
		</p>
		<p>Icons based on the trademark:</p>
		<img src={iconthick} alt="parktrackr-logo" css={logoStyle} />
		<p>Thin icons based on ½ width trademark:</p>
		<img src={iconsthin} alt="parktrackr-logo" css={logoStyle} />
		<div style={{ height: '100px' }} />
	</div>
);

export default Iconography;
