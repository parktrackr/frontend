import React from 'react';
/* eslint-disable max-len */
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import typograph from '../../../../assets/brand/brandbook/typograph.png';
import typoapp from '../../../../assets/brand/brandbook/typo-app.png';

const intro = css`
	font-family: 'Poppins', sans-serif;
`;

const logoStyle = css`
	width: 700px;
`;
const Typography: React.FC = () => (
	<div>
		<h3 css={intro}>
			The typography represents our informal way of communication.
		</h3>
		<h3 css={intro} style={{ marginTop: '50px' }}>
			GLOBAL
		</h3>
		<p>
			We make use of two different fonts, Poppins (headings) and Roboto
			(body).
		</p>
		<img src={typograph} alt="parktrackr-logo" css={logoStyle} />
		<h3 css={intro} style={{ marginTop: '50px' }}>
			APPLICATION
		</h3>
		<img src={typoapp} alt="parktrackr-logo" css={logoStyle} />
		<div style={{ height: '100px' }} />
	</div>
);

export default Typography;
