export { default as About } from './About';
export { default as Branding } from './Branding';
export { default as Color } from './Color';
export { default as Image } from './Image';
export { default as Iconography } from './Iconography';
export { default as Logo } from './Logo';
export { default as Trademark } from './Trademark';
export { default as Typography } from './Typography';
