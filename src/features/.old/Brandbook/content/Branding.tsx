import React from 'react';
/* eslint-disable max-len */
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import thechip from '../../../../assets/brand/brandbook/thechip.png';

const intro = css`
	font-family: 'Poppins', sans-serif;
`;

const logoStyle = css`
	width: 250px;
`;

const Branding: React.FC = () => (
	<div style={{ width: '60vw' }}>
		<h3 css={intro}>What makes ParkTrackr different.</h3>
		<h3 css={intro} style={{ marginTop: '50px' }}>
			THE CHIP
		</h3>
		<p>
			To make sure that our brand is as recognizable as can be with our
			potential customers, we created a ‘brand element’ on the basis of
			our trademark. ‘The Chip’ comes back in most of our expressions. The
			shapes of The Chip will only be used in elements that have enough
			space to justify the usage of it. The chip will always occupy 15% of
			the element, where there is also space for whitespace. The width of
			the whitespace is the width of our trademark.
		</p>
		<br />
		<br />
		<img src={thechip} alt="parktrackr-logo" css={logoStyle} />
		<h3 css={intro} style={{ marginTop: '50px' }}>
			COMMUNICATION
		</h3>
		<p>
			In our communication, other than our tone of voice, we also
			implement other branding methods. Symbols who are associated with
			our brand identity or entertainment can also be used in certain
			contexts to show our identity better. The vertical bar (|) can be
			used in showing the hierarchy. For example to show that something is
			the paragraph in a chapter.
		</p>
		<p>
			For Example:
			<b>| Chapter 1</b>
		</p>
		<div style={{ height: '100px' }} />
	</div>
);

export default Branding;
