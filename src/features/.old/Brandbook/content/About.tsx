import React from 'react';
/* eslint-disable max-len */
import { css, jsx } from '@emotion/core';
/** @jsx jsx */

const intro = css`
	font-family: 'Poppins', sans-serif;
`;

const About: React.FC = () => (
	<div style={{ width: '60vw' }}>
		<h3 css={intro}>The identity explaination of ParkTrackr.</h3>
		<h3 css={intro} style={{ marginTop: '50px' }}>
			INTRODUCTION
		</h3>
		<p>
			This is the Brand Identity Book of ParkTrackr. This is the guardian
			of the identity of ParkTrackr. In this, the usage of the brand will
			be explained. Topics like color, typography, logo, image, icons and
			the usage of those are also recorded.
		</p>
		<p>
			Other than that we also explain our way of communication, our core
			values and the tone of voices.
		</p>
		<h3 css={intro} style={{ marginTop: '50px' }}>
			BRAND STATEMENT
		</h3>
		<p>
			We are an online platform on which both theme park enthusiasts as
			theme park goers can go to share and find information on theme
			parks, but also meet-up with other members to create a fun having
			community in which the hobby of theme parks can flourish. Through
			working with the customer on a very close level we can develop the
			platform in a way where it is constantly expanding and growing so
			that it always fulfills the wishes of the customer.
		</p>
		<ul>
			<li>
				<b>Co-creation /</b>
				The customer is part of our team. This helps us to learn more
				about the customer and make our product better.
			</li>
			<li>
				<b>Flexibility /</b>
				We make use of a flexible work method, in which a solution comes
				from multiple development periods so that we can deliver the
				highest quality as possible.
			</li>
			<li>
				<b>Innovative /</b>
				We want to be the leading service on the market, this means
				experimenting with and implementing the most modern technologies
				and ideas. This makes sure that our product is unique, safe and
				reliable.
			</li>
			<li>
				<b>Playful / </b>
				We want to have fun with the service and the topics the service
				touches. This means that the brand should also project the same
				feeling as what we want to achieve.
			</li>
		</ul>
		<h3 css={intro} style={{ marginTop: '50px' }}>
			TONE OF VOICE
		</h3>
		<ul>
			<li>
				<b>Informal / </b>
				Because we see the customer as a part of the product, we will
				approach the customer in an informal way. Other than that an
				anecdote here and there is desirable to keep it light.
			</li>
			<li>
				<b>Advisory / </b>
				Since we want to provide the knowledge and experience we have
				and our customers have to other users in this area, we want to
				keep an advisory tone of voice.
			</li>
			<li>
				<b>Uplifting / </b>
				Because we are focussing on an entertainment product and
				providing a service in that category, we also want to project
				the same feeling with an uplifting tone of voice.
			</li>
			<li>
				<b>Ambitious / </b>
				Since there are always things that can be better, we also want
				to make that clear to the customers. We want to make the product
				better with the help of the customers and an ambitious tone of
				voice can help with that.
			</li>
		</ul>
		<div style={{ height: '100px' }} />
	</div>
);

export default About;
