import React from 'react';
/* eslint-disable max-len */
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import logo from '../../../../assets/brand/logo/parktrackr.png';
import lines from '../../../../assets/brand/brandbook/parktrackr-lines.png';
import whitelogo from '../../../../assets/brand/logo/parktrackr-white.png';
import photowm from '../../../../assets/brand/brandbook/photowithwm.jpg';
import wrongOne from '../../../../assets/brand/brandbook/wrong1.png';
import wrongTwo from '../../../../assets/brand/brandbook/wrong2.png';
import printlogo from '../../../../assets/brand/brandbook/printlogo.png';

const intro = css`
	font-family: 'Poppins', sans-serif;
`;

const logoStyle = css`
	width: 700px;
`;

const logoColor = css`
	display: flex;
	flex-direction: row;
`;

const colorStyle = css`
	padding: 40px;
	background-color: #d43e36;
	color: white;
	margin-left: 30px;
	width: 200px;
	font-family: 'Poppins', sans-serif;
`;
const Logo: React.FC = () => (
	<div>
		<h3 css={intro}>
			With the arc of a rollercoaster track, our logo is trying to reflect
			the excitement of our users.
		</h3>
		<img src={logo} alt="parktrackr-logo" css={logoStyle} />
		<h3 css={intro} style={{ marginTop: '50px' }}>
			COLOR
		</h3>
		<p>
			Our primary logo is valencia, with grey on a white background. This
			is to create a calm, but exciting feel.
		</p>
		<div css={logoColor}>
			<img
				src={logo}
				alt="parktrackr-logo"
				css={logoStyle}
				style={{ width: '400px' }}
			/>
			<div css={colorStyle}>
				<b>
					Valencia
					<br />
				</b>
				RGB: 212, 62, 54
				<br />
				CMYK: 110, 91, 93, 2
				<br />
				HSL: 3, 75, 50
				<br />
			</div>
		</div>
		<h3 css={intro} style={{ marginTop: '50px' }}>
			CLEARING SPACE
		</h3>
		<p>
			You can use the ‘a’ from the logo as a measuring instrument for the
			whitespace.
		</p>
		<img
			src={lines}
			alt="parktrackr-logo-clear"
			style={{ width: '700px' }}
		/>
		<h3 css={intro} style={{ marginTop: '50px' }}>
			LOGO ON COLOR
		</h3>
		<p>
			In certain situations you can use the logo on a colored background
		</p>
		<img
			src={whitelogo}
			alt="parktrackr-logo-clear"
			style={{
				width: '500px',
				backgroundColor: '#D43E36',
				padding: '60px',
			}}
		/>
		<h3 css={intro} style={{ marginTop: '50px' }}>
			VIDEO WATERMARK
		</h3>
		<p>
			On very few occasions, the logo can appear in videos. One such
			occasion the logo has to be white.
		</p>
		<img
			src={photowm}
			alt="parktrackr-logo-clear"
			style={{ width: '500px' }}
		/>
		<h3 css={intro} style={{ marginTop: '50px' }}>
			AVOID
		</h3>
		<img
			src={wrongOne}
			alt="parktrackr-logo-clear"
			style={{ width: '500px' }}
		/>
		<br />
		<img
			src={wrongTwo}
			alt="parktrackr-logo-clear"
			style={{ width: '500px' }}
		/>
		<h3 css={intro} style={{ marginTop: '50px' }}>
			OTHER USAGE
		</h3>
		<p>
			The logo should always be visible. To make sure that the logo/emblem
			does not become unrecognizable small,
			<br />
			the logo/emblem can not be smaller than the specifics written down
			here.
		</p>
		<img
			src={printlogo}
			alt="parktrackr-logo-clear"
			style={{ width: '500px' }}
		/>
		<div style={{ height: '100px' }} />
	</div>
);

export default Logo;
