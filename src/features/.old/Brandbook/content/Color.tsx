import React from 'react';
/* eslint-disable max-len */
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import primcolor from '../../../../assets/brand/brandbook/primcolor.png';
import seccolor from '../../../../assets/brand/brandbook/seccolor.png';
import greycolors from '../../../../assets/brand/brandbook/greycolors.png';
import greencolor from '../../../../assets/brand/brandbook/greencolor.png';
import yellowcolor from '../../../../assets/brand/brandbook/yellowcolor.png';

const intro = css`
	font-family: 'Poppins', sans-serif;
`;

const logoStyle = css`
	width: 700px;
`;

const Color: React.FC = () => (
	<div style={{ width: '700px' }}>
		<h3 css={intro}>
			The colors for us were handpicked to sparkle excitement and joy.
		</h3>
		<h3 css={intro} style={{ marginTop: '50px' }}>
			PRIMARY & SECONDARY
		</h3>
		<p>ParkTrackr uses 1 primary color and 1 secondary color.</p>
		<img src={primcolor} alt="parktrackr-logo" css={logoStyle} />
		<img src={seccolor} alt="parktrackr-logo" css={logoStyle} />
		<h3 css={intro} style={{ marginTop: '50px' }}>
			ACCENT COLORS
		</h3>
		<p>
			ParkTrackr uses different accent colors for different user cases.
			These are based on the purpose.
		</p>
		<img src={greencolor} alt="parktrackr-logo" css={logoStyle} />
		<img src={yellowcolor} alt="parktrackr-logo" css={logoStyle} />
		<h3 css={intro} style={{ marginTop: '50px' }}>
			GREYS
		</h3>
		<p>
			ParkTrackr uses different shades for their greys, so that they fit
			the primary and secondary colors. They are based upon Valencia.
		</p>
		<img src={greycolors} alt="parktrackr-logo" css={logoStyle} />
		<div style={{ height: '100px' }} />
	</div>
);

export default Color;
