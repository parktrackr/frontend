import React from 'react';
/* eslint-disable max-len */
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import rightuse from '../../../../assets/brand/brandbook/rightuse.png';
import wronguse from '../../../../assets/brand/brandbook/wronguse.png';
import wrongusetwo from '../../../../assets/brand/brandbook/wrongusetwo.png';

const intro = css`
	font-family: 'Poppins', sans-serif;
`;

const logoStyle = css`
	width: 700px;
`;
const Image: React.FC = () => (
	<div>
		<h3 css={intro}>
			Even though our image content is created mostly by the users, we do
			have guidelines.
		</h3>
		<h3 css={intro} style={{ marginTop: '50px' }}>
			PHOTOGRAPHY
		</h3>
		<p>
			To make sure that the image of the brand is consistent and fits
			ParkTrackr, this part shows which images can be used.
		</p>
		<p>
			Furthermore, the image in our expressions should always meet the
			three rules of thumb:
		</p>
		<ol>
			<li>
				Photos should always be of high quality, both in image quality
				as composition.
			</li>
			<li>Photos need to offer support to the topic.</li>
			<li>Photos should be realistic.</li>
		</ol>
		<p>
			Don’t use abstract photos who do not have any link between text and
			image.
		</p>
		<h3 css={intro} style={{ marginTop: '50px' }}>
			RIGHT USAGE
		</h3>
		<img src={rightuse} alt="parktrackr-logo" css={logoStyle} />
		<h3 css={intro} style={{ marginTop: '50px' }}>
			WRONG USAGE
		</h3>
		<img src={wronguse} alt="parktrackr-logo" css={logoStyle} />

		<br />
		<img src={wrongusetwo} alt="parktrackr-logo" css={logoStyle} />
		<div style={{ height: '100px' }} />
	</div>
);

export default Image;
