import React from 'react';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import { Switch, Route } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { Header, Footer } from './elements';
import { Assets, Landing } from './features';

const brandbookStyle = css`
	display: flex;
	flex-direction: column;
	position: relative;
`;

const contentStyle = css`
	padding-bottom: 200px;
	padding-top: 70px;
	width: 100vw;
	min-height: 100vh;
	position: relative;
`;

const Brandbook: React.FC = () => {
	return (
		<div css={brandbookStyle}>
			<Helmet>
				<title>ParkTrackr - Brand Identity Book</title>
			</Helmet>
			<Header />
			<div css={contentStyle}>
				<Switch>
					<Route exact path="/brandbook" component={Landing} />
					<Route path="/brandbook/assets" component={Assets} />
				</Switch>
			</div>
			<Footer />
		</div>
	);
};

export default Brandbook;
