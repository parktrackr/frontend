import React from 'react';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */

const assetsInfo = [
	{
		title: 'Logo',
		subtitle: 'OUR BRAND SIGNATURE',
	},
	{
		title: 'Trademark',
		subtitle: 'TRANSCENDING LANGUAGE AND CULTURE',
	},
	{
		title: 'Colors',
		subtitle: 'COLORS TO FEED YOUR SOUL',
	},
	{
		title: 'Typography',
		subtitle: 'THE WRITTEN WORD IS STRONG',
	},
	{
		title: 'Image',
		subtitle: 'THE LOOK OF THE REAL WORLD',
	},
	{
		title: 'About',
		subtitle: 'THE CONCEPT WE CALL PARKTRACKR',
	},
	{
		title: 'Branding',
		subtitle: 'WE ARE THE EDGE',
	},
	{
		title: 'Iconography',
		subtitle: 'THE SMALL PART IN OUR IDENTITY',
	},
];

type AssetsProps = {
	history: {
		location: {
			pathname: string;
		};
	};
};

const moduleWrapper = css`
	display: flex;
	-ms-flex-flow: row wrap;
	flex-flow: row wrap;
`;

const moduleSeperator = css`
	width: 50%;
	position: relative;
	margin: 20px 0;
	padding: 0 2px;
	display: flex;
	flex-direction: column;
`;

const moduleHeader = css`
	position: relative;
	max-width: 80%;
	margin-bottom: 10px;
	font-family: 'Poppins', sans-serif;
	font-weight: 700;
	font-size: 36px;
	line-height: 40px;
`;

const moduleSubHeader = css`
	position: relative;
	max-width: 80%;
	margin-bottom: 40px;
	font-family: 'Roboto', sans-serif;
	font-weight: 400;
	min-height: 44px;
	font-size: 18px;
	line-height: 22px;
	padding-left: 10px;
	border-left: 2px solid #e50914;
`;

const ModuleHeader: React.FC<AssetsProps> = ({ history }) => {
	const { location } = history;
	const { pathname } = location;
	let info = {
		title: 'Title',
		subtitle: 'Subtitle',
	};

	if (pathname === '/brandbook/assets/logo') {
		info = assetsInfo[0];
	} else if (pathname === '/brandbook/assets/trademark') {
		info = assetsInfo[1];
	} else if (pathname === '/brandbook/assets/colors') {
		info = assetsInfo[2];
	} else if (pathname === '/brandbook/assets/typography') {
		info = assetsInfo[3];
	} else if (pathname === '/brandbook/assets/image') {
		info = assetsInfo[4];
	} else if (pathname === '/brandbook/assets') {
		info = assetsInfo[5];
	} else if (pathname === '/brandbook/assets/branding') {
		info = assetsInfo[6];
	} else if (pathname === '/brandbook/assets/iconography') {
		info = assetsInfo[7];
	}

	return (
		<div css={moduleWrapper}>
			<div css={moduleSeperator}>
				<h2 css={moduleHeader}>
					<span>{info.title}</span>
				</h2>
				<span css={moduleSubHeader}>{info.subtitle}</span>
			</div>
		</div>
	);
};

export default ModuleHeader;
