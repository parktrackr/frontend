import React from 'react';
import { Link } from 'react-router-dom';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import emblem from '../../../../assets/brand/trademark/emblem.png';

const headerStyle = css`
	height: 70px;
	width: 100%;
	display: flex;
	justify-content: flex-start;
	align-items: center;
	line-height: 1;
	border-bottom: 1px solid #e6dede;
	background-color: white;
	z-index: 10;
	position: fixed;
	overflow: hidden;
	top: 0;
`;

const homePageLinkStyle = css`
	width: 20vw;
	display: flex;
	justify-content: flex-start;
	align-items: center;
	font-family: 'Poppins', sans-serif;
	font-weight: 600;
	text-decoration: none;
	color: #2a2121;
	&:hover {
		color: #d43e36;
	}
	z-index: 10;
`;

const emblemStyle = css`
	margin-right: 12px;
	width: 48px;
	padding-left: 50px;
`;

const navLinkStyle = css`
	width: 20vw;
	padding-left: 40px;
	display: flex;
	flex-direction: column;
	border-left: 1px solid #e6dede;
	font-family: 'Roboto', sans-serif;
	color: #2a2121;
	text-decoration: none;
	&:hover {
		color: #d43e36;
	}
`;

const navLinklabelOne = css`
	font-size: 10px;
	font-weight: 200;
	line-height: 1.5;
`;

const Header: React.FC = () => (
	<header css={headerStyle}>
		<Link to="/brandbook" css={homePageLinkStyle}>
			<img src={emblem} css={emblemStyle} alt="parktrackr-icon" />
			<div>
				<span>ParkTrackr</span>
				<br />
				<span>Brandbook</span>
			</div>
		</Link>
		<Link to="/brandbook/assets" css={navLinkStyle}>
			<span css={navLinklabelOne}>Explore</span>
			<span>Brand Assets</span>
		</Link>
	</header>
);

export default Header;
