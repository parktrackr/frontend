import React from 'react';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import { NavLink } from 'react-router-dom';

type Page = {
	path: string;
	name: string;
};

type SidebarProps = {
	pages: Page[];
	pathname: string;
};

type SidebarLinkProps = {
	path: string;
	name: string;
	pathname: string;
};

const menuItem = css`
	position: relative;
	margin-bottom: 14px;
	border-left: 2px solid transparent;
`;

const menuLink = css`
	color: #141414;
	font-family: 'Poppins', sans-serif;
	font-weight: 700;
	padding-left: 10px;
	border-left: 2px solid transparent;
	display: block;
	line-height: 24px;
	text-decoration: none;
	&:active {
		border-color: #e50914;
		color: #e50914;
	}
`;

const menuWrapper = css`
	border-right: 1px solid rgba(34, 31, 31, 0.15);
`;

const menuNav = css`
	position: sticky;
	top: 40px;
	padding: 80px 40px 15px 0;
	width: 230px;
	display: block;
`;
const menuTitle = css`
	font-family: 'Poppins', sans-serif;
	font-weight: 400;
	display: block;
	margin: 10px 0 30px;
	font-size: 18px;
	color: rgba(34, 31, 31, 0.4);
`;

const Sidebar: React.FC<SidebarProps> = ({ pages, pathname }) => (
	<div css={menuWrapper}>
		<nav css={menuNav}>
			<strong css={menuTitle}>Brand Assets</strong>
			{pages.map((item: Page) => (
				<SidebarLink
					path={item.path}
					name={item.name}
					pathname={pathname}
				/>
			))}
		</nav>
	</div>
);

const SidebarLink: React.FC<SidebarLinkProps> = ({ path, name, pathname }) => (
	<div css={menuItem}>
		<NavLink
			to={path}
			css={menuLink}
			style={{
				borderColor: pathname !== path ? 'white' : 'red',
			}}
		>
			{name}
		</NavLink>
	</div>
);

export default Sidebar;
