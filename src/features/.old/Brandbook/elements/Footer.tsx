import React from 'react';
import { Link } from 'react-router-dom';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import logo from '../../../../assets/brand/logo/parktrackr.png';

const footerStyle = css`
	position: absolute;
	bottom: 0;
	height: 200px;
	width: 100%;
	padding-top: 30px;
	padding-bottom: 30px;
	background-color: #efe9e9;
	border-top: 1px solid #e6dede;
	overflow: visible;
`;

const footerWrapper = css`
	padding-left: 40px;
	display: flex;
	flex-direction: column;
	margin-top: 32px;
`;

const logoStyle = css`
	height: 64px;
	width: 150px;
	margin-bottom: 32px;
`;

const termsStyle = css`
	font-size: 11px;
	color: #968686;
	font-family: 'Roboto', sans-serif;
`;

const linkStyle = css`
	font-weight: 700;
	border-bottom: 1px solid #968686;
	color: #968686;
	text-decoration: none;
`;

const Footer: React.FC = () => (
	<footer css={footerStyle}>
		<div css={footerWrapper}>
			<img src={logo} css={logoStyle} alt="parktrackr-logo" />
			<p css={termsStyle}>
				All uses of ParkTrackr materials are subject to
				<Link to="/brandbook" css={linkStyle}>
					Terms and Conditions.
				</Link>
			</p>
		</div>
	</footer>
);

export default Footer;
