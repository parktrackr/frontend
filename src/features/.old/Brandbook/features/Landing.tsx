import React from 'react';
import { Link } from 'react-router-dom';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import logo from '../../../../assets/brand/logo/parktrackr.png';
import emblem from '../../../../assets/brand/trademark/emblem.png';

const mainContentWrapper = css`
	padding: 0 40px;
	position: relative;
	top: 70px;
	display: flex;
	flex-direction: row;
`;

const container = css`
	height: auto;
	padding: 40px 0;
	align-self: flex-end;
	z-index: 2;
	width: 50%;
	margin: 0 auto;
	display: flex;
	flex-direction: column;
	position: relative;
	margin-left: 10vw;
`;

const containerTitle = css`
	font-family: 'Poppins', sans-serif;
	max-width: 600px;
	line-height: 55px;
	font-size: 50px;
	font-weight: 700;
`;

const containerText = css`
	font-family: 'Roboto', sans-serif;
	max-width: 290px;
	font-size: 28px;
	line-height: 28px;
	margin-top: 20px;
	margin-bottom: 60px;
	font-weight: 400;
	min-height: 44px;
	padding-left: 10px;
	border-left: 2px solid #d43e36;
`;

const containerButton = css`
	margin-right: 8px;
	color: #d43e36;
	text-decoration: none;
	overflow: hidden;
	transition: color 0.25s cubic-bezier(0.215, 0.61, 0.355, 1);
	font-family: Bold-font, Arial;
	font-weight: 700;
	position: relative;
	display: inline-block;
	padding: 12px 25px;
	font-size: 14px;
	text-align: center;
	line-height: 18px;
	background: #fff;
	outline: none;
	border: none;
	background-color: #fff;
	border: 2px solid #d43e36;
	&:hover {
		color: white;
		box-shadow: 0 5px 16px rgba(229, 9, 20, 0.3);
		background-color: #d43e36;
	}
	max-width: 130px;
`;

const logoStyle = css`
	height: 128px;
	width: 300px;
	position: relative;
	display: block;
	margin: 90px 0 40px;
`;

const imageWrapper = css`
	overflow: hidden;
	position: absolute;
	right: 0;
	bottom: -200px;
	opacity: 0.5;
`;

const imageContainer = css`
	margin-right: -400px;
	overflow: hidden;
	width: 1200px;
	height: 1000px;
	background: url(${emblem});
	background-position: left;
	background-size: 100% 100%;
`;

const Content: React.FC = () => (
	<div css={mainContentWrapper}>
		<div css={container}>
			<h1 css={containerTitle}>Welcome to the ParkTrackr Brand Site</h1>
			<div css={containerText}>THE HOME OF OUR BRAND</div>
			<Link to="/brandbook/assets" css={containerButton}>
				EXPLORE ASSETS
			</Link>
			<img src={logo} css={logoStyle} alt="parktrackr-logo" />
		</div>
		<div css={imageWrapper}>
			<div css={imageContainer} />
		</div>
	</div>
);

export default Content;
