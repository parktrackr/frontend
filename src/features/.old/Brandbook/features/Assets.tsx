import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { css, jsx } from '@emotion/core';
import bg from '../../../../assets/brand/brandbook/headerbg.jpg';
import { Sidebar } from '../elements';
import ModuleHeader from '../elements/ModuleHeader';
import {
	About,
	Branding,
	Color,
	Iconography,
	Image,
	Logo,
	Trademark,
	Typography,
} from '../content';
/** @jsx jsx */

const pages = [
	{
		path: '/brandbook/assets',
		name: 'About',
	},
	{
		path: '/brandbook/assets/logo',
		name: 'Logo',
	},
	{
		path: '/brandbook/assets/trademark',
		name: 'Trademark',
	},
	{
		path: '/brandbook/assets/iconography',
		name: 'Iconography',
	},
	{
		path: '/brandbook/assets/typography',
		name: 'Typography',
	},
	{
		path: '/brandbook/assets/colors',
		name: 'Colors',
	},
	{
		path: '/brandbook/assets/image',
		name: 'Image',
	},
	{
		path: '/brandbook/assets/branding',
		name: 'Branding',
	},
];

type AssetsProps = {
	history: {
		location: {
			pathname: string;
		};
	};
};

const pageWrapper = css`
	position: relative;
	display: flex;
	flex-direction: column;
	overflow: hidden;
	width: 100vw;
`;

const headerTitle = css`
	margin-bottom: 20px;
	font-family: 'Poppins', sans-serif;
	font-weight: 700;
	font-size: 56px;
	line-height: 60px;
	position: absolute;
	left: 100px;
	bottom: 80px;
`;

const headerSubTitle = css`
	margin-bottom: 20px;
	font-family: 'Poppins', sans-serif;
	font-weight: 400;
	font-size: 16px;
	line-height: 60px;
	position: absolute;
	left: 100px;
	bottom: 30px;
`;

const headerContainer = css`
	max-width: none;
	align-items: flex-start;
	width: 100vw;
	height: 250px;
	padding-bottom: 40px;
	color: #fff;
	background: #121212;
	background-size: cover;
	background-position: 50%;
	overflow: hidden;
	justify-content: flex-end;
	display: flex;
	position: relative;
	padding: 0 80px;
`;

const contentWrapper = css`
	display: flex;
	flex-direction: row;
	min-height: calc(100vh - 300px);
	width: 100%;
	max-width: 1420px;
	padding: 0 80px;
	margin: 0 auto;
`;

const contentArea = css`
	position: relative;
	width: 100%;
	min-height: 100vh;
	display: flex;
	flex-direction: column;
	padding: 50px 0 50px 40px;
`;

const Assets: React.FC<AssetsProps> = ({ history }) => {
	const { location } = history;
	const { pathname } = location;
	return (
		<div css={pageWrapper}>
			<div css={headerContainer}>
				<img src={bg} style={{ width: '170%' }} alt="header-pattern" />
				<h1 css={headerTitle}>Brand Assets</h1>
				<span css={headerSubTitle}>
					The elements of our brands visual identity
				</span>
			</div>
			<div css={contentWrapper}>
				<Sidebar pages={pages} pathname={pathname} />
				<div css={contentArea}>
					<ModuleHeader history={history} />
					<Switch>
						<Route
							exact
							path="/brandbook/assets"
							component={About}
						/>
						<Route path="/brandbook/assets/logo" component={Logo} />
						<Route
							path="/brandbook/assets/trademark"
							component={Trademark}
						/>
						<Route
							path="/brandbook/assets/iconography"
							component={Iconography}
						/>
						<Route
							path="/brandbook/assets/typography"
							component={Typography}
						/>
						<Route
							path="/brandbook/assets/colors"
							component={Color}
						/>
						<Route
							path="/brandbook/assets/image"
							component={Image}
						/>
						<Route
							path="/brandbook/assets/branding"
							component={Branding}
						/>
					</Switch>
				</div>
			</div>
		</div>
	);
};

export default Assets;
