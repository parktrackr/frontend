import React, { useState } from 'react';
import { Helmet } from 'react-helmet';

const Contact = () => {
	const [username, setName] = useState<string>('');
	const [email, setEmail] = useState<string>('');
	const [message, setMessage] = useState<string>('');

	const handleSubmit = (e: any) => {
		e.preventDefault();
		console.log(username, email, message);
	};
	return (
		<div>
			<Helmet>
				<title>Contact - ParkTrackr</title>
			</Helmet>
			<form
				id="contact-form"
				onSubmit={e => handleSubmit(e)}
				method="POST"
			>
				<div>
					<p>Name</p>
					<input
						type="text"
						onChange={e => setName(e.target.value)}
					/>
				</div>
				<div>
					<p>Email address</p>
					<input
						type="email"
						aria-describedby="emailHelp"
						onChange={e => setEmail(e.target.value)}
					/>
				</div>
				<div>
					<p>Message</p>
					<textarea
						rows={5}
						onChange={e => setMessage(e.target.value)}
					/>
				</div>
				<button type="submit">Submit</button>
			</form>
			<div>
				<p>
					If the form does not work, send an email to:
					rhcoldenkotte@icloud.com
				</p>
			</div>
		</div>
	);
};

export default Contact;
