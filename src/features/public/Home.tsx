import React, { useEffect, useContext } from 'react';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import { Helmet } from 'react-helmet';

// Elements
import { Link } from 'react-router-dom';

// Contexts
import { HeaderContext } from '../../contexts/HeaderContext';

// Reducer Types
import { Types as headerTypes } from '../../reducers/headerReducer';

// Assets
import stockphoto from '../../assets/graphics/stockphoto.jpg';
import emblem from '../../assets/brand/trademark/emblem-white.png';

const Home: React.FC = () => {
	const { dispatch: headerDispatch } = useContext(HeaderContext);
	useEffect(() => {
		headerDispatch({
			type: headerTypes.LoadTitle,
			payload: {
				text: 'Home',
			},
		});
		headerDispatch({
			type: headerTypes.LoadSubTitle,
			payload: {
				text: null,
			},
		});
		headerDispatch({
			type: headerTypes.LoadColor,
			payload: {
				text: '#FA857C',
			},
		});
		headerDispatch({
			type: headerTypes.LoadSubColor,
			payload: {
				text: '#E37870',
			},
		});
		headerDispatch({
			type: headerTypes.LoadTitleColor,
			payload: {
				text: '#FFFFFF',
			},
		});
		headerDispatch({
			type: headerTypes.LoadIcon,
			payload: {
				text: emblem,
			},
		});
		headerDispatch({
			type: headerTypes.LoadSubHeader,
			payload: {
				text: null,
			},
		});
	}, [headerDispatch]);

	return (
		<div>
			<Helmet>
				<title>ParkTrackr</title>
			</Helmet>
			<div css={imageWrapper}>
				<div css={shadowOverlay} />
				<div css={textContainer}>
					<p css={textOverlay}>Welcome to</p>
					<p css={textOverlay}>ParkTrackr!</p>
				</div>
				<img src={stockphoto} css={imageStyle} alt="park" />
			</div>
			<div css={lineStyle}> </div>
			<div css={dividerStyle}>
				<button
					css={buttonStyle}
					type="button"
					onClick={() => console.log('Button Clicked')}
				>
					TRY IT
					<div css={triangle} />
				</button>
			</div>
			<div css={lineStyleTwo} />
			<div css={tempLinks}>
				<Link to="/signin">Sign in</Link>
				<Link to="/signup">Create Account</Link>
			</div>
		</div>
	);
};

const imageWrapper = css`
	position: relative;
	height: 350px;
	width: 100%;
	overflow: hidden;
	z-index: 1;
`;

const imageStyle = css`
	height: 100%;
	width: 100%;
	object-fit: cover;
`;

const lineStyle = css`
	width: 100%;
	height: 6px;
	background-color: #ffffff;
	position: relative;
`;

const lineStyleTwo = css`
	width: 100%;
	height: 6px;
	background-color: #ffffff;
	position: relative;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
`;

const shadowOverlay = css`
	position: absolute;
	background-image: radial-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0));
	width: 100%;
	height: 100%;
	z-index: 3;
`;
const textContainer = css`
	position: absolute;
	z-index: 10;
	height: 100%;
	width: 100%;
	display: flex;
	justify-content: center;
	align-items: center;
	flex-direction: column;
`;

const textOverlay = css`
	color: white;
	font-family: 'Poppins';
	font-weight: 700;
	font-size: 3em;
	margin: -10px;
`;

const dividerStyle = css`
	background-color: #707070;
	width: 100%;
	height: 36px;
	position: relative;
	display: flex;
	justify-content: center;
	align-items: center;
`;

const buttonStyle = css`
	background-color: #fa857c;
	margin-top: -48px;
	z-index: 8;
	width: 60%;
	height: 48px;
	border-radius: 3px;
	border: none;
	color: white;
	font-family: 'Poppins';
	font-weight: 700;
	font-size: 1.5em;
	position: relative;
`;

const triangle = css`
	width: 0;
	height: 0;
	border-bottom: 15px solid rgba(0, 0, 0, 0.15);
	border-left: 15px solid transparent;
	position: absolute;
	bottom: 5px;
	right: 5px;
`;

const tempLinks = css`
	padding: 20px;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
`;

export default Home;
