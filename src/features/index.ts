/* eslint-disable import/prefer-default-export */

// admin
export { default as Admin } from './admin/Admin';

// public
export { default as Home } from './public/Home';
export { default as Contact } from './public/Contact';

// user
export { default as UserDashboard } from './user/UserDashboard/UserDashboard';
export { default as UserInformation } from './user/UserDashboard/UserInformation';
export { default as Friendships } from './user/UserDashboard/Friendships';
export { default as FriendRequests } from './user/UserDashboard/FriendRequests';
export { default as ParkCount } from './user/UserDashboard/ParkCount';
export { default as RideCount } from './user/UserDashboard/RideCount';
export { default as AchievementList } from './user/UserDashboard/AchievementList';
export { default as SignIn } from './user/SignIn';
export { default as SignInHelp } from './user/SignInHelp';
export { default as SignUp } from './user/SignUp';
export { default as Profile } from './user/profile/Profile';
export { default as ProfileRideCount } from './user/profile/ProfileRideCount';
export { default as ProfileParkCount } from './user/profile/ProfileParkCount';
export { default as ProfileAchievements } from './user/profile/ProfileAchievements';

// park
export { default as Park } from './park/Park';
export { default as ParkList } from './park/ParkList';

// ride
export { default as Ride } from './ride/Ride';
export { default as RideList } from './ride/RideList';
