import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
// Apollo
import { ApolloProvider, ApolloConsumer } from 'react-apollo';
import { ApolloProvider as ApolloHookProvider } from '@apollo/react-hooks';
import { client } from './apollo/apollo';
// Context Providers
import UserContextProvider from './contexts/UserContext';
import HeaderContextProvider from './contexts/HeaderContext';

import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
	<ApolloProvider client={client}>
		<ApolloHookProvider client={client}>
			<ApolloConsumer>
				{(): JSX.Element => (
					<UserContextProvider>
						<HeaderContextProvider>
							<BrowserRouter>
								<App />
							</BrowserRouter>
						</HeaderContextProvider>
					</UserContextProvider>
				)}
			</ApolloConsumer>
		</ApolloHookProvider>
	</ApolloProvider>,
	document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
