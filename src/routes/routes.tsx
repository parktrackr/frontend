import React from 'react';
import { Route, Switch } from 'react-router-dom';

import ScrollInToView from '../utils/ScrollIntoView';

import {
	Home,
	UserDashboard,
	UserInformation,
	FriendRequests,
	Profile,
	SignIn,
	SignInHelp,
	SignUp,
	Park,
	ParkList,
	ParkCount,
	RideCount,
	Friendships,
	AchievementList,
	Ride,
	RideList,
	Contact,
	Admin,
	ProfileRideCount,
	ProfileParkCount,
	ProfileAchievements,
} from '../features';

const Routes: React.FC = () => {
	return (
		<ScrollInToView>
			<Switch>
				<Route exact path="/" component={Home} />
				<Route path="/admin" component={Admin} />
				<Route exact path="/dashboard" component={UserDashboard} />
				<Route path="/dashboard/parkcount" component={ParkCount} />
				<Route path="/dashboard/ridecount" component={RideCount} />
				<Route path="/dashboard/user" component={UserInformation} />
				<Route path="/dashboard/friendships" component={Friendships} />
				<Route
					path="/dashboard/achievements"
					component={AchievementList}
				/>
				<Route
					path="/dashboard/friendrequests"
					component={FriendRequests}
				/>
				<Route exact path="/user/:userId" component={Profile} />
				<Route
					path="/user/:userId/ridecount"
					component={ProfileRideCount}
				/>
				<Route
					path="/user/:userId/parkcount"
					component={ProfileParkCount}
				/>
				<Route
					path="/user/:userId/achievements"
					component={ProfileAchievements}
				/>
				<Route
					exact
					path="/signin"
					render={(props): JSX.Element => <SignIn {...props} />}
				/>
				<Route exact path="/signin/help" component={SignInHelp} />
				<Route
					path="/signup"
					render={(props): JSX.Element => <SignUp {...props} />}
				/>
				<Route path="/park/:parkId/ride/:rideId" component={Ride} />
				<Route exact path="/rides" component={RideList} />
				<Route path="/park/:parkId" component={Park} />
				<Route exact path="/parks" component={ParkList} />
				<Route path="/contact" component={Contact} />
				<Route
					component={(): JSX.Element => (
						<div>
							<p>Page not Found 404</p>
						</div>
					)}
				/>
			</Switch>
		</ScrollInToView>
	);
};

export default Routes;
