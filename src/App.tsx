import React, { useContext } from 'react';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */

// Routes
import { Switch, Route } from 'react-router-dom';
import { HeaderContext } from './contexts/HeaderContext';
import Routes from './routes/routes';

// Elements
import { Header, Footer, Menu } from './elements';

import { Admin } from './features';

const DefaultRoute = () => {
	const { state: headerState } = useContext(HeaderContext);
	const { subHeader } = headerState;
	return (
		<div>
			<Header />
			<div css={wrapper}>
				<div
					css={innerWrapper}
					style={{ paddingTop: subHeader === null ? '11vh' : '17vh' }}
				>
					<Routes />
					<div css={pushStyle} />
				</div>
			</div>
			<Menu />
			<Footer />
		</div>
	);
};

const AdminRoute = () => (
	<div>
		<Switch>
			<Route path="/admin" component={Admin} />
		</Switch>
	</div>
);

const App: React.FC = () => {
	return (
		<Switch>
			<Route path="/admin" component={AdminRoute} />
			<Route component={DefaultRoute} />
		</Switch>
	);
};

const wrapper = css`
	padding-bottom: 0;
	min-height: 76vh;
`;

const innerWrapper = css`
	min-height: 100%;
`;

const pushStyle = css`
	height: 24vh;
`;

export default App;
