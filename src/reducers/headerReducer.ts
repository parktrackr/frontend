// TODO Refactor headerReducer
type ActionMap<M extends { [index: string]: any }> = {
	[Key in keyof M]: M[Key] extends undefined
		? {
				type: Key;
		  }
		: {
				type: Key;
				payload: M[Key];
		  };
};

export enum Types {
	LoadTitle = 'LOAD_TITLE',
	LoadSubTitle = 'LOAD_SUBTITLE',
	LoadColor = 'LOAD_COLOR',
	LoadSubColor = 'LOAD_SUB_COLOR',
	LoadTitleColor = 'LOAD_TITLE_COLOR',
	LoadIcon = 'LOAD_ICONS',
	LoadSubHeader = 'LOAD_SUB_HEADER',
}

type TitlePayload = {
	[Types.LoadTitle]: {
		text: string;
	};
	[Types.LoadSubTitle]: {
		text: string | null;
	};
	[Types.LoadColor]: {
		text: string;
	};
	[Types.LoadSubColor]: {
		text: string;
	};
	[Types.LoadTitleColor]: {
		text: string;
	};
	[Types.LoadIcon]: {
		text: string;
	};
	[Types.LoadSubHeader]: {
		text: string | null;
	};
};

export type titleActions = ActionMap<TitlePayload>[keyof ActionMap<
	TitlePayload
>];

export const titleReducer = (state: string | any, action: titleActions) => {
	switch (action.type) {
		case Types.LoadTitle: {
			if (action.payload.text) {
				return action.payload.text;
			}
			return state;
		}
		default:
			return state;
	}
};

type SubTitlePayload = {
	[Types.LoadTitle]: {
		text: string;
	};
	[Types.LoadSubTitle]: {
		text: string | null;
	};
	[Types.LoadColor]: {
		text: string;
	};
	[Types.LoadSubColor]: {
		text: string;
	};
	[Types.LoadTitleColor]: {
		text: string;
	};
	[Types.LoadIcon]: {
		text: string;
	};
	[Types.LoadSubHeader]: {
		text: string | null;
	};
};

export type subTitleActions = ActionMap<SubTitlePayload>[keyof ActionMap<
	SubTitlePayload
>];

export const subTitleReducer = (state: any, action: subTitleActions) => {
	switch (action.type) {
		case Types.LoadSubTitle: {
			return action.payload.text;
		}
		default:
			return state;
	}
};

type ColorPayLoad = {
	[Types.LoadTitle]: {
		text: string;
	};
	[Types.LoadSubTitle]: {
		text: string | null;
	};
	[Types.LoadColor]: {
		text: string;
	};
	[Types.LoadSubColor]: {
		text: string;
	};
	[Types.LoadTitleColor]: {
		text: string;
	};
	[Types.LoadIcon]: {
		text: string;
	};
	[Types.LoadSubHeader]: {
		text: string | null;
	};
};

export type headerColorActions = ActionMap<ColorPayLoad>[keyof ActionMap<
	ColorPayLoad
>];

export const headerColorReducer = (
	state: string | any,
	action: headerColorActions
) => {
	switch (action.type) {
		case Types.LoadColor: {
			if (action.payload.text) {
				return action.payload.text;
			}
			return state;
		}
		default:
			return state;
	}
};

type subColorPayLoad = {
	[Types.LoadTitle]: {
		text: string;
	};
	[Types.LoadSubTitle]: {
		text: string | null;
	};
	[Types.LoadColor]: {
		text: string;
	};
	[Types.LoadSubColor]: {
		text: string;
	};
	[Types.LoadTitleColor]: {
		text: string;
	};
	[Types.LoadIcon]: {
		text: string;
	};
	[Types.LoadSubHeader]: {
		text: string | null;
	};
};

export type subColorActions = ActionMap<subColorPayLoad>[keyof ActionMap<
	subColorPayLoad
>];

export const subColorReducer = (
	state: string | any,
	action: subColorActions
) => {
	switch (action.type) {
		case Types.LoadSubColor: {
			if (action.payload.text) {
				return action.payload.text;
			}
			return state;
		}
		default:
			return state;
	}
};

type titleColorPayLoad = {
	[Types.LoadTitle]: {
		text: string;
	};
	[Types.LoadSubTitle]: {
		text: string | null;
	};
	[Types.LoadColor]: {
		text: string;
	};
	[Types.LoadSubColor]: {
		text: string;
	};
	[Types.LoadTitleColor]: {
		text: string;
	};
	[Types.LoadIcon]: {
		text: string;
	};
	[Types.LoadSubHeader]: {
		text: string | null;
	};
};

export type titleColorActions = ActionMap<titleColorPayLoad>[keyof ActionMap<
	titleColorPayLoad
>];

export const titleColorReducer = (
	state: string | any,
	action: titleColorActions
) => {
	switch (action.type) {
		case Types.LoadTitleColor: {
			if (action.payload.text) {
				return action.payload.text;
			}
			return state;
		}
		default:
			return state;
	}
};

type iconPayLoad = {
	[Types.LoadTitle]: {
		text: string;
	};
	[Types.LoadSubTitle]: {
		text: string | null;
	};
	[Types.LoadColor]: {
		text: string;
	};
	[Types.LoadSubColor]: {
		text: string;
	};
	[Types.LoadTitleColor]: {
		text: string;
	};
	[Types.LoadIcon]: {
		text: string;
	};
	[Types.LoadSubHeader]: {
		text: string | null;
	};
};

export type iconActions = ActionMap<iconPayLoad>[keyof ActionMap<iconPayLoad>];

export const iconReducer = (state: string | any, action: iconActions) => {
	switch (action.type) {
		case Types.LoadIcon: {
			if (action.payload.text) {
				return action.payload.text;
			}
			return state;
		}
		default:
			return state;
	}
};

type subHeaderPayLoad = {
	[Types.LoadTitle]: {
		text: string;
	};
	[Types.LoadSubTitle]: {
		text: string | null;
	};
	[Types.LoadColor]: {
		text: string;
	};
	[Types.LoadSubColor]: {
		text: string;
	};
	[Types.LoadTitleColor]: {
		text: string;
	};
	[Types.LoadIcon]: {
		text: string;
	};
	[Types.LoadSubHeader]: {
		text: string | null;
	};
};

export type subHeaderActions = ActionMap<subHeaderPayLoad>[keyof ActionMap<
	subHeaderPayLoad
>];

export const subHeaderReducer = (
	state: string | any,
	action: subHeaderActions
) => {
	switch (action.type) {
		case Types.LoadSubHeader: {
			return action.payload.text;
		}
		default:
			return state;
	}
};
