// TODO Refactor userReducer
type ActionMap<M extends { [index: string]: any }> = {
	[Key in keyof M]: M[Key] extends undefined
		? {
				type: Key;
		  }
		: {
				type: Key;
				payload: M[Key];
		  };
};

export enum Types {
	Load = 'LOAD_USER',
	Store = 'STORE_USER',
	ToggleLoading = 'TOGGLE_LOADING',
}

type loadingPayLoad = {
	[Types.Load]: {
		token: string | null;
	};
	[Types.Store]: {
		data: any;
	};
	[Types.ToggleLoading]: {
		bool: boolean;
	};
};

export type loadingActions = ActionMap<loadingPayLoad>[keyof ActionMap<
	loadingPayLoad
>];

export const loadingReducer = (state: any, action: loadingActions) => {
	switch (action.type) {
		case Types.ToggleLoading: {
			return action.payload.bool;
		}
		default:
			return state;
	}
};

type TokenPayload = {
	[Types.Load]: {
		token: string | null;
	};
	[Types.Store]: {
		data: any;
	};
	[Types.ToggleLoading]: {
		bool: boolean;
	};
};

export type tokenActions = ActionMap<TokenPayload>[keyof ActionMap<
	TokenPayload
>];

export const tokenReducer = (state: string | any, action: tokenActions) => {
	switch (action.type) {
		case Types.Load: {
			if (action.payload.token !== state) {
				return action.payload.token;
			}
			return state;
		}
		default:
			return state;
	}
};

type UserPayload = {
	[Types.Load]: {
		token: string | null;
	};
	[Types.Store]: {
		data: any;
	};
	[Types.ToggleLoading]: {
		bool: boolean;
	};
};

export type userActions = ActionMap<UserPayload>[keyof ActionMap<UserPayload>];

export const userReducer = (state: any, action: userActions) => {
	switch (action.type) {
		case Types.Store: {
			if (action.payload.data) {
				return action.payload.data;
			}
			return state;
		}
		default:
			return state;
	}
};
