import React from 'react';
/** @jsx jsx */
import { css, jsx } from '@emotion/core';

import searchIcon from '../../assets/icons/search.svg';
import Results from './Results';

type TSearchBar = {
	filter: string;
	handleSearch(value: string): void;
	results: boolean;
	toggleResults(bool: boolean): void;
};

const SearchBar: React.FC<TSearchBar> = ({
	handleSearch,
	filter,
	results,
	toggleResults,
}) => {
	return (
		<div css={SearchBarBox} id="search-bar">
			{results ? (
				<div>
					<div css={SearchBarFormBox}>
						<form css={SearchForm}>
							<input
								type="text"
								placeholder="Search..."
								css={InputField}
								defaultValue={filter}
								onChange={e => {
									handleSearch(e.target.value);
								}}
							/>
						</form>
						<Results
							toggleResults={toggleResults}
							filter={filter}
						/>
					</div>
					<div
						css={overlayStyle}
						onClick={() => toggleResults(false)}
						onKeyDown={() => toggleResults(true)}
						tabIndex={-1}
						role="button"
						aria-label="overlayClickable"
					/>
				</div>
			) : (
				<div
					css={searchButton}
					onClick={() => toggleResults(true)}
					onKeyDown={() => toggleResults(true)}
					tabIndex={-1}
					role="button"
				>
					<img src={searchIcon} alt="search" css={searchIconStyle} />
				</div>
			)}
		</div>
	);
};

const SearchBarBox = css`
	vertical-align: top;
	float: left;
	position: relative;
	width: 100%;
	height: 60px;
`;

const SearchBarFormBox = css`
	position: absolute;
	height: 60px;
	width: 100%;
	margin-top: 10vh;
	z-index: 10;
`;

const SearchForm = css`
	display: flex;
	justify-content: center;
	align-items: center;
	height: 60px;
	margin: auto;
`;

const InputField = css`
	z-index: 10;
	position: relative;
	margin: 0 5px;
	height: 60px;
	color: rgba(0, 0, 0, 0.5);
	font-size: 1.1em;
	font-weight: bold;
	padding: 8px;
	width: 80%;
	height: 30px;
	margin-top: 5px;
	border: 0;
	border-radius: 3px;
	text-align: center;
	outline-width: 0;
	transition: width 0.4s ease-in-out;
	&::placeholder {
		transition: color 0.2s ease-in-out;
		color: rgba(0, 0, 0, 0.3);
	}
	&:focus {
		transition: width 0.4s ease-in-out;
		border-bottom: 2px solid rgba(0, 0, 0, 0.2);
	}
`;

const searchButton = css`
	width: 48px;
	background-color: rgba(0, 0, 0, 0.4);
	border-radius: 100%;
	right: 8px;
	top: 8px;
	position: absolute;
	padding: 10px 4px;
	display: flex;
	align-items: center;
	justify-content: center;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
	cursor: pointer;
`;

const searchIconStyle = css`
	width: 75%;
`;

const overlayStyle = css`
	height: 100vh;
	width: 100vw;
	background-color: rgba(0, 0, 0, 0.5);
`;

export default SearchBar;
