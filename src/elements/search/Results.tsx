import React, { useEffect } from 'react';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import { useQuery } from '@apollo/react-hooks';

// Elements
import { Link } from 'react-router-dom';

// queries
import { PARKS_QUERY_BY_ORIGINALNAME } from '../../apollo/graphql/queries/park';
import { RIDES_QUERY_BY_ORIGINALNAME } from '../../apollo/graphql/queries/ride';

type TResults = {
	filter: string;
	toggleResults(bool: boolean): void;
};

const Results: React.FC<TResults> = ({ filter, toggleResults }) => {
	useEffect(() => {
		const handleClick = (e: any) => {
			if (
				!e.path.some(
					(el: any) => el.id === 'results' || el.id === 'search-bar'
				)
			) {
				document.removeEventListener('mousedown', handleClick);
				toggleResults(false);
			}
		};
		document.addEventListener('mousedown', handleClick);
	});
	const { error: parksQueryError, data: parks } = useQuery<any>(
		PARKS_QUERY_BY_ORIGINALNAME,
		{
			variables: { filter },
			onError() {
				console.error(parksQueryError);
			},
		}
	);

	const { error: ridesQueryError, data: rides } = useQuery<any>(
		RIDES_QUERY_BY_ORIGINALNAME,
		{
			variables: { filter },
			onError() {
				console.error(ridesQueryError);
			},
		}
	);

	if (filter === '') {
		return null;
	}

	return (
		<div css={resultContainer} id="results">
			<div>
				{parks && parks.parks.length > 0 ? <p>Parks:</p> : null}
				<div css={parksResults}>
					{parks &&
						parks.parks &&
						parks.parks.map((park: any) => (
							<Link
								key={park.id}
								onClick={() => toggleResults(false)}
								to={`/park/${park.slug}`}
							>
								{park.originalName}
							</Link>
						))}
				</div>
				{rides && rides.rides.length > 0 ? <p>Rides:</p> : null}
				<div css={ridesResults}>
					{rides &&
						rides.rides &&
						rides.rides.map((ride: any) => (
							<Link
								key={ride.id}
								onClick={() => toggleResults(false)}
								to={`/park/${ride.park.slug}/ride/${ride.slug}`}
							>
								{ride.originalName}
							</Link>
						))}
				</div>
			</div>
		</div>
	);
};

const resultContainer = css`
	width: 80%;
	height: auto;
	background-color: white;
	z-index: 10;
	margin: 0 auto;
	margin-top: 10px;
	padding: 8px;
	border-radius: 3px;
	z-index: 10;
	box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14),
		0 3px 1px -2px rgba(0, 0, 0, 0.12), 0 1px 5px 0 rgba(0, 0, 0, 0.2);
`;

const parksResults = css`
	display: flex;
	flex-direction: column;
`;

const ridesResults = css`
	display: flex;
	flex-direction: column;
`;

export default Results;
