import React, { useContext, useState } from 'react';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
import { Textfit } from 'react-textfit';

// Contexts
import { HeaderContext } from '../../contexts/HeaderContext';

// Components
import SearchBar from '../search/SearchBar';

const Header: React.FC = () => {
	const [filter, setFilter] = useState('');
	const [results, toggleResults] = useState(false);

	const { state: userState } = useContext(HeaderContext);

	const handleSearch = (value: string) => {
		setFilter(value);
		toggleResults(true);
	};

	const {
		title,
		subTitle,
		headerColor,
		subColor,
		titleColor,
		icon,
		subHeader,
	} = userState;

	return (
		<header css={headerCSS}>
			<div css={headerStyling} style={{ backgroundColor: headerColor }}>
				<div css={headerContainer}>
					<div
						css={iconContainer}
						role="button"
						onClick={() => console.log('Icon Clicked')}
						onKeyDown={() => console.log('Icon Clicked')}
						tabIndex={-1}
						style={{ backgroundColor: subColor }}
					>
						<img css={emblemStyle} src={icon} alt="page-emblem" />
						<div css={iconTriangle} />
					</div>
					<div css={headerTitleContainer}>
						<div css={headerSubTitle}>
							<Textfit
								style={{
									position: 'absolute',
									bottom: 0,
									color: titleColor,
									width: '140px',
									height: '100%',
								}}
								mode="single"
								forceSingleModeWidth={false}
								max={16}
							>
								{subTitle}
							</Textfit>
						</div>
						<div css={titleContainer}>
							<Textfit
								mode="multi"
								min={20}
								max={28}
								style={{
									height: '100%',
									lineHeight: 1.2,
									width: '180px',
									color: titleColor,
								}}
							>
								{title}
							</Textfit>
						</div>
					</div>
					<div
						css={clickContainer}
						role="button"
						onClick={() => console.log('Header Clicked')}
						onKeyDown={() => console.log('Header Clicked')}
						tabIndex={-1}
						aria-label="HeaderClickable"
					/>
					<div css={triangleContainer}>
						<div css={triangle} />
					</div>
				</div>
			</div>
			{subHeader ? (
				<div css={subHeaderContainer}>
					<p css={subHeaderText}>{subHeader}</p>
				</div>
			) : (
				<div>
					<SearchBar
						handleSearch={handleSearch}
						filter={filter}
						results={results}
						toggleResults={toggleResults}
					/>
				</div>
			)}
		</header>
	);
};

const headerCSS = css`
	height 16vh;
	position: fixed;
	width: 100%;
	z-index: 10;
	top: 0;
`;

const headerStyling = css`
	height: 11vh;
	border-bottom-left-radius: 3px;
	border-bottom-right-radius: 3px;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
`;

const headerContainer = css`
	display: flex;
	flex-direction: row;
	padding-top: 2vh;
	height: 7.5vh;
	z-index: 10;
	position: relative;
`;

const triangleContainer = css`
	position: absolute;
	bottom: 0;
	right: 0;
	margin-right: 12px;
	display: flex;
	flex-direction: column;
	justify-content: flex-end;
`;

const triangle = css`
	width: 0;
	height: 0;
	border-bottom: 20px solid rgba(0, 0, 0, 0.15);
	border-left: 20px solid transparent;
`;

const headerTitleContainer = css`
	position: relative;
	bottom: 0;
	display: flex;
	flex-direction: column;
	justify-content: flex-end;
	margin-left: 10px;
	width: 200px;
`;

const titleContainer = css`
	font-family: 'Poppins';
	margin: 0;
	font-weight: 700;
	height: 40px;
`;

const headerSubTitle = css`
	margin: 0;
	font-family: 'Poppins';
	color: #5f5f5f;
	margin-bottom: -4px;
	height: 40px;
	position: relative;
`;

const iconContainer = css`
	width: 120px;
	height: 110px;
	margin-left: 12px;
	border-radius: 3px;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
	position: relative;
	display: flex;
	align-items: center;
	justify-content: center;
	z-index: 10;
`;

const iconTriangle = css`
	width: 0;
	height: 0;
	border-bottom: 20px solid rgba(0, 0, 0, 0.15);
	border-left: 20px solid transparent;
	position: absolute;
	bottom: 8px;
	right: 8px;
`;

const emblemStyle = css`
	max-width: 70%;
`;

const clickContainer = css`
	position: absolute;
	width: 100%;
	height: 11vh;
	margin-top: -2vh;
`;

const subHeaderContainer = css`
	background-color: white;
	height: 6vh;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
	width: 100%;
	z-index: 1;
	border-bottom-left-radius: 3px;
	border-bottom-right-radius: 3px;
`;

const subHeaderText = css`
	text-align: left;
	margin: 0;
	margin-left: 142px;
	padding-top: 8px;
`;

export default Header;
