import React, { useState } from 'react';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import {
	FacebookIcon,
	FacebookShareButton,
	TwitterIcon,
	TwitterShareButton,
} from 'react-share';

const Footer: React.FC = () => {
	const [shareMenu, toggleShareMenu] = useState(false);
	return (
		<footer>
			<div css={footerStyling}>
				<div css={iconContainer}>
					<div
						css={shareButtonContainer}
						onClick={() => toggleShareMenu(true)}
						onKeyDown={() => toggleShareMenu(true)}
						tabIndex={-1}
						role="button"
					>
						<object
							data="https://parktrackr.ams3.digitaloceanspaces.com/assets/share-white.svg"
							type="image/svg+xml"
							css={statsIconStyle}
							aria-label="statistics"
						/>
					</div>
					<div css={iconWrapper}>
						<i
							className="fa fa-twitter-square"
							aria-hidden="true"
							css={iconStyling}
							onClick={() =>
								window.open(
									'https://twitter.com/parktrackr',
									'_blank'
								)
							}
						/>
						<i
							className="fa fa-facebook-official"
							aria-hidden="true"
							css={iconStyling}
							onClick={() =>
								window.open(
									'https://www.facebook.com/parktrackr/',
									'_blank'
								)
							}
						/>
					</div>
				</div>
			</div>
			{shareMenu ? (
				<div css={shareMenuContainer}>
					<div
						css={overlayStyle}
						onClick={() => toggleShareMenu(false)}
						onKeyDown={() => toggleShareMenu(false)}
						tabIndex={-1}
						role="button"
						aria-label="overlayClickable"
					/>
					<div css={buttonContainer}>
						<FacebookShareButton
							url="http://parktrackr.com"
							quote="Join ParkTrackr!"
							css={shareButton}
						>
							<FacebookIcon size={64} />
						</FacebookShareButton>
						<TwitterShareButton
							url="http://parktrackr.com"
							title="Join ParkTrackr!"
							css={shareButton}
						>
							<TwitterIcon size={64} />
						</TwitterShareButton>
					</div>
				</div>
			) : null}
		</footer>
	);
};
// TODO: Fix footer stickyness
const footerStyling = css`
	width: 100vw;
	bottom: 0;
	height: 24vh;
	background-color: #707070;
	border-top-left-radius: 5px;
	border-top-right-radius: 5px;
	box-shadow: 0 -1px 3px rgba(0, 0, 0, 0.12), 0 -1px 2px rgba(0, 0, 0, 0.24);
`;

const iconContainer = css`
	display: flex;
	min-width: 100%;
	justify-content: space-between;
`;

const iconWrapper = css`
	margin-right: 20px;
	margin-top: 16px;
`;

const iconStyling = css`
	color: white;
	font-size: 3em;
	margin: 8px;
`;

const statsIconStyle = css`
	min-width: 100%;
	min-height: 100%;
	pointer-events: none;
`;

const shareButtonContainer = css`
	height: 40px;
	width: 40px;
	margin-top: 24px;
	margin-left: 20px;
`;

const shareMenuContainer = css`
	position: fixed;
	top: 0;
	z-index: 9;
	display: flex;
	justify-content: center;
	height: 100vh;
	width: 100vw;
	align-items: center;
`;

const overlayStyle = css`
	position: fixed;
	height: 100vh;
	width: 100vw;
	background-color: rgba(0, 0, 0, 0.5);
`;

const shareButton = css`
	z-index: 10;
	margin: 4px;
`;

const buttonContainer = css`
	display: flex;
	justify-content: center;
	height: 72px;
`;
export default Footer;
