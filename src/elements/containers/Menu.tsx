import React, { useState, useContext } from 'react';
import { css, jsx } from '@emotion/core';
/** @jsx jsx */
import { Link } from 'react-router-dom';

import menuButtonIcon from '../../assets/icons/menuButton.svg';
import homeIcon from '../../assets/icons/home.svg';
import achievementIcon from '../../assets/icons/achievement.svg';
import statsIcon from '../../assets/icons/stats.svg';
import backIcon from '../../assets/icons/back.svg';
import usersIcon from '../../assets/icons/users.svg';
import { UserContext } from '../../contexts/UserContext';

const Menu: React.FC = () => {
	const [active, setActive] = useState(false);
	const { state: userState } = useContext(UserContext);
	if (active) {
		return (
			<div css={overlayStyle}>
				<div css={menuWrapper}>
					<div css={firstRow}>
						<div css={creditsAndAchievements}>
							<Link
								to={
									userState.currentUser !== null
										? '/dashboard/ridecount'
										: '/signin'
								}
								onClick={() => setActive(false)}
								css={creditsButton}
							>
								<img
									src="https://parktrackr.ams3.digitaloceanspaces.com/assets/credits/credits-white.svg"
									alt="credits"
									css={creditsIconStyle}
								/>
								<span css={buttonText}>Credits</span>
								<div css={triangle} />
							</Link>
							<Link
								to={
									userState.currentUser !== null
										? '/dashboard/achievements'
										: '/signin'
								}
								onClick={() => setActive(false)}
								css={achievementsButton}
							>
								<img
									src={achievementIcon}
									alt="achievements"
									css={achievementIconStyle}
								/>
								<span css={buttonText}>Achievements</span>
								<div css={triangle} />
							</Link>
						</div>
						<Link
							to={
								userState.currentUser !== null
									? '/dashboard'
									: '/'
							}
							onClick={() => setActive(false)}
							css={homeButton}
						>
							<img
								src={homeIcon}
								alt="home"
								css={homeIconStyle}
							/>
							<span css={buttonText}>Home</span>
							<div css={triangle} />
						</Link>
					</div>
					<div css={secondRow}>
						<div css={statsAndBack}>
							<Link
								to="/"
								onClick={() => setActive(false)}
								css={statsButton}
							>
								<img
									src={statsIcon}
									alt="stats"
									css={statsIconStyle}
								/>
								<span css={buttonText}>Statistics</span>
								<div css={triangle} />
							</Link>
							<div
								css={backButton}
								onClick={() => setActive(false)}
								onKeyDown={() => setActive(false)}
								tabIndex={-1}
								role="button"
							>
								<img
									src={backIcon}
									alt="back"
									css={backIconStyle}
								/>
								<span css={buttonText}>Back</span>
								<div css={triangle} />
							</div>
						</div>
						<Link
							to={
								userState.currentUser !== null
									? '/dashboard/friendships'
									: '/signin'
							}
							onClick={() => setActive(false)}
							css={friendsButton}
						>
							<img
								src={usersIcon}
								alt="friends"
								css={usersIconStyle}
							/>
							<span css={buttonText}>Friends</span>
							<div css={triangle} />
						</Link>
					</div>
				</div>
			</div>
		);
	}
	return (
		<div
			css={MenuDiv}
			onClick={() => setActive(true)}
			onKeyDown={() => setActive(false)}
			tabIndex={-1}
			role="button"
		>
			<img src={menuButtonIcon} css={menuicon} alt="menu" />
		</div>
	);
};

const MenuDiv = css`
	width: 48px;
	background-color: rgba(0, 0, 0, 0.4);
	border-radius: 100%;
	bottom: 24px;
	left: 24px;
	margin-bottom: 24px;
	position: sticky;
	padding: 14px 8px;
	display: flex;
	align-items: center;
	justify-content: center;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
	cursor: pointer;
	z-index: 8;
`;

const menuicon = css`
	width: 70%;
`;

const overlayStyle = css`
	position: fixed;
	top: 0;
	left: 0;
	height: 100vh;
	width: 100vw;
	background-color: rgba(255, 255, 255, 0.75);
	z-index: 10;
	text-decoration: none;
`;

const menuWrapper = css`
	padding: 12px;
	height: 100%;
	box-sizing: border-box;
`;

const firstRow = css`
	width: 100%;
	height: 60%;
	display: flex;
	flex-direction: row;
	padding-bottom: 12px;
	box-sizing: border-box;
`;

const secondRow = css`
	width: 100%;
	height: 40%;
	display: flex;
	flex-direction: row;
	box-sizing: border-box;
`;

const creditsAndAchievements = css`
	height: 100%;
	width: 60%;
	display: flex;
	flex-direction: column;
	padding-right: 12px;
`;

const creditsButton = css`
	width: 100%;
	height: 60%;
	background-color: #7cd6fa;
	box-sizing: border-box;
	margin-bottom: 12px;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
	border-radius: 4px;
	text-decoration: none;
	position: relative;
`;

const creditsIconStyle = css`
	width: 60%;
`;

const achievementsButton = css`
	width: 100%;
	background-color: #6adc9a;
	box-sizing: border-box;
	height: 40%;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
	border-radius: 4px;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	text-decoration: none;
	position: relative;
`;

const achievementIconStyle = css`
	width: 30%;
`;

const homeButton = css`
	height: 100%;
	width: 40%;
	background-color: #fa857c;
	box-sizing: border-box;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
	border-radius: 4px;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	text-decoration: none;
	position: relative;
`;

const homeIconStyle = css`
	width: 50%;
`;

const statsAndBack = css`
	height: 100%;
	display: flex;
	flex-direction: column;
	width: 90%;
	padding-right: 12px;
`;

const statsButton = css`
	width: 100%;
	height: 60%;
	margin-bottom: 12px;
	background-color: #968686;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
	border-radius: 4px;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	text-decoration: none;
	position: relative;
`;

const statsIconStyle = css`
	width: 40%;
`;

const backButton = css`
	width: 100%;
	height: 40%;
	background-color: #feb9b3;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	text-decoration: none;
	position: relative;
`;

const backIconStyle = css`
	width: 30%;
`;

const friendsButton = css`
	width: 100%;
	height: 100%;
	background-color: #ffe182;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
	border-radius: 4px;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	text-decoration: none;
	position: relative;
`;

const usersIconStyle = css`
	width: 60%;
`;

const buttonText = css`
	margin-top: 12px;
	font-family: Poppins;
	font-weight: 700;
	color: white;
	font-size: 1.5em;
`;

const triangle = css`
	position: absolute;
	bottom: 8px;
	right: 8px;
	width: 0;
	height: 0;
	border-bottom: 20px solid rgba(0, 0, 0, 0.15);
	border-left: 20px solid transparent;
`;

export default Menu;
