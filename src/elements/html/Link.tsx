import React from 'react';
import { Link as LinkImport } from 'react-router-dom';

type LinkProps = {
	path?: string;
	text?: string;
};

const Link: React.FC<LinkProps> = ({ path, text }) => {
	if (!path || !text) return null;

	return <LinkImport to={path}>{text}</LinkImport>;
};

export default Link;
