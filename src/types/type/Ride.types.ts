import { TPark } from './Park.types';

export type TRide = {
	id: string;
	originalName: string;
	slug: string;
	park: TPark;
	tempImage: string;
};
