export type TPark = {
	id: string;
	originalName: string;
	slug: string;
	tempImage: string;
};

export type TParkCount = {
	id: string;
	park: any;
	data: any;
};

export type TParkQuery = {
	park: any;
};
