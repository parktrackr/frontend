import { ApolloClient } from 'apollo-client';
import { NormalizedCacheObject } from 'apollo-cache-inmemory';

export type TClient = ApolloClient<NormalizedCacheObject>;
