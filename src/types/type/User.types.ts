import { TParkCount } from './Park.types';

export type TCurrentUserQuery = {
	me: {
		id: string;
		username: string;
		parkVisits: TParkCount[] | null;
		rideVisits: any;
	};
};

export type TAddParkToCountMutation = {
	addParkToCount: any;
};

export type SignInMutationProps = {
	signIn: {
		token: string;
	};
};
