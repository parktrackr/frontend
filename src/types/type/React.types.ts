import { string } from 'prop-types';

export type THistory = {
	push(url: string): void;
};

export type TMatch = {
	isExact: boolean;
	params: {
		[key: string]: string;
	};
	path: string;
	url: string;
};
