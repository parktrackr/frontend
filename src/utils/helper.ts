// TODO Find out if this helper function is necessary
export const isClient = () => {
	if (typeof window !== 'undefined' && window.document) {
		return true;
	}
	return false;
};

const getKeys = (array: []) => {
	return Object.keys(array).map((key: any) => {
		return {
			key,
			groupedData: array[key],
		};
	});
};

// TODO Give types to result and currentValyue
export const groupByKey = (array: [], key: string) => {
	const newArray = array.reduce((result: any, currentValue: any) => {
		if (!result[currentValue[key]]) {
			// eslint-disable-next-line no-param-reassign
			result[currentValue[key]] = {
				data: [],
				[key]: currentValue[key],
			};
		}
		// eslint-disable-next-line no-param-reassign
		result[currentValue[key]] = {
			...result[currentValue[key]],
			data: [...result[currentValue[key]].data, currentValue],
		};
		return result;
	}, []);
	return getKeys(newArray);
};

export const groupBySubKey = (array: [], key: string, subkey: string) => {
	const newArray = array.reduce((result: any, currentValue: any) => {
		if (!result[currentValue[key][subkey]]) {
			// eslint-disable-next-line no-param-reassign
			result[currentValue[key][subkey]] = {
				data: [],
				[key]: currentValue[key],
			};
		}
		// eslint-disable-next-line no-param-reassign
		result[currentValue[key][subkey]] = {
			...result[currentValue[key][subkey]],
			data: [...result[currentValue[key][subkey]].data, currentValue],
		};
		return result;
	}, []);
	return getKeys(newArray);
};

export const convertToSelect = (array: [], value: string, label: string) => {
	return array
		.map((p: any) => ({
			value: p[value],
			label: p[label],
		}))
		.sort((a: any, b: any) => (a.label > b.label ? 1 : -1));
};
