/* eslint-disable react/destructuring-assignment */
import { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';

class ScrollIntoView extends PureComponent {
	componentDidMount = () => window.scrollTo(0, 0);

	componentDidUpdate = (prevProps: any) => {
		// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
		// @ts-ignore
		if (this.props.location !== prevProps.location) window.scrollTo(0, 0);
	};

	render = () => this.props.children;
}

export default withRouter<any, any>(ScrollIntoView);
