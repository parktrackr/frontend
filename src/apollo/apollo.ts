import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-boost';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';

const httpLink = createHttpLink({
	uri: 'http://parktrackr.com/graphql',
});

const authLink = setContext((_, { headers }) => {
	const token = localStorage.getItem('x-token');
	return {
		headers: {
			...headers,
			authorization: token ? `Bearer ${token}` : '',
		},
	};
});

export const client = new ApolloClient({
	link: authLink.concat(httpLink),
	cache: new InMemoryCache(),
});
