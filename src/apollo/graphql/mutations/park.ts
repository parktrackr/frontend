import { gql } from 'apollo-boost';

export const CREATE_PARK = gql`
	mutation CreatePark($name: String!, $tempImage: String!) {
		createPark(name: $name, tempImage: $tempImage) {
			id
			slug
		}
	}
`;

export const DELETE_PARK = gql`
	mutation DeletePark($id: ID!) {
		deletePark(id: $id)
	}
`;

export const UPDATE_PARK = gql`
	mutation UpdatePark($id: ID!, $name: String!, $tempImage: String!) {
		updatePark(id: $id, name: $name, tempImage: $tempImage) {
			id
		}
	}
`;
