import { gql } from 'apollo-boost';

export const CREATE_ACHIEVEMENT = gql`
	mutation CreateAchievement(
		$name: String!
		$color: String!
		$icon: String!
		$rideIds: [String!]
	) {
		createAchievement(
			name: $name
			color: $color
			icon: $icon
			rideIds: $rideIds
		) {
			id
		}
	}
`;

export const DELETE_ACHIEVEMENT = gql`
	mutation DeleteAchievement($id: ID!) {
		deleteAchievement(id: $id)
	}
`;

export const UPDATE_ACHIEVEMENT = gql`
	mutation UpdateAchievement(
		$id: ID!
		$name: String!
		$color: String!
		$icon: String!
		$rideIds: [String!]
	) {
		updateAchievement(
			id: $id
			name: $name
			color: $color
			icon: $icon
			rideIds: $rideIds
		) {
			id
		}
	}
`;
