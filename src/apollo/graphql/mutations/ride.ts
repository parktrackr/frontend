import { gql } from 'apollo-boost';

export const CREATE_RIDE = gql`
	mutation CreateRide(
		$parkId: String!
		$name: String!
		$rideCategoryIds: [String!]!
		$rideTypeIds: [String!]!
		$tempImage: String!
	) {
		createRide(
			parkId: $parkId
			name: $name
			rideCategoryIds: $rideCategoryIds
			rideTypeIds: $rideTypeIds
			tempImage: $tempImage
		) {
			id
			slug
		}
	}
`;

export const DELETE_RIDE = gql`
	mutation DeleteRide($id: ID!) {
		deleteRide(id: $id)
	}
`;

export const UPDATE_RIDE = gql`
	mutation UpdateRide(
		$id: ID!
		$parkId: String!
		$name: String!
		$intensity: Int!
		$height: Int
		$length: Int
		$rideCategoryIds: [String!]!
		$rideTypeIds: [String!]!
		$tempImage: String!
	) {
		updateRide(
			id: $id
			parkId: $parkId
			name: $name
			intensity: $intensity
			height: $height
			length: $length
			rideCategoryIds: $rideCategoryIds
			rideTypeIds: $rideTypeIds
			tempImage: $tempImage
		) {
			id
			slug
		}
	}
`;

export const CREATE_RIDE_CATEGORY = gql`
	mutation CreateRideCategory($name: String!, $color: String!) {
		createRideCategory(name: $name, color: $color) {
			id
		}
	}
`;

export const DELETE_RIDE_CATEGORY = gql`
	mutation DeleteRideCategory($id: ID!) {
		deleteRideCategory(id: $id)
	}
`;

export const UPDATE_RIDE_CATEGORY = gql`
	mutation UpdateRideCATEGORY($id: ID!, $name: String!, $color: String!) {
		updateRideCategory(id: $id, name: $name, color: $color) {
			id
		}
	}
`;
