import { gql } from 'apollo-boost';

export const CREATE_FRIENDSHIP = gql`
	mutation CreateFriendship($userId: String!) {
		createFriendship(userId: $userId) {
			id
		}
	}
`;

export const ACCEPT_FRIENDSHIP = gql`
	mutation AcceptFriendship($friendshipId: String!) {
		acceptFriendship(friendshipId: $friendshipId) {
			id
		}
	}
`;
