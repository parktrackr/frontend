import { gql } from 'apollo-boost';

export const SIGN_IN = gql`
	mutation SignIn($login: String!, $password: String!) {
		signIn(login: $login, password: $password) {
			token
		}
	}
`;

export const SIGN_UP = gql`
	mutation CreateUser(
		$username: String!
		$email: String!
		$password: String!
	) {
		createUser(username: $username, email: $email, password: $password) {
			token
		}
	}
`;

export const ADD_PARK_TO_COUNT = gql`
	mutation AddParkToCount($parkId: ID!) {
		addParkToCount(parkId: $parkId) {
			id
			username
			parkVisits {
				id
				park {
					id
					originalName
				}
			}
		}
	}
`;

export const ADD_RIDE_TO_COUNT = gql`
	mutation AddRideToCount($rideId: ID!) {
		addRideToCount(rideId: $rideId) {
			id
			username
			rideVisits {
				id
				ride {
					id
					originalName
				}
			}
		}
	}
`;
