import { gql } from 'apollo-boost';

export const PARK_QUERY_BY_ID = gql`
	query Park($id: ID!) {
		park(id: $id) {
			id
			originalName
			slug
			tempImage
		}
	}
`;

export const PARK_QUERY_BY_SLUG = gql`
	query Park($slug: String!) {
		park(slug: $slug) {
			id
			originalName
			slug
			tempImage
			rides {
				id
				slug
				originalName
				tempImage
				rideCategories {
					id
					originalName
					color
					icon
				}
			}
		}
	}
`;

export const PARKS_QUERY_BY_ORIGINALNAME = gql`
	query Parks($filter: String!) {
		parks(filter: $filter) {
			id
			originalName
			slug
		}
	}
`;

export const PARKS_QUERY = gql`
	query Parks {
		parks {
			id
			originalName
			slug
			tempImage
			rides {
				id
				originalName
			}
		}
	}
`;
