import { gql } from 'apollo-boost';

export const RIDE_QUERY_BY_SLUG = gql`
	query Ride($slug: String!) {
		ride(slug: $slug) {
			id
			originalName
			slug
			intensity
			height
			length
			tempImage
			park {
				id
				originalName
				slug
			}
			rideCategories {
				id
				color
				icon
			}
		}
	}
`;

export const RIDE_QUERY_BY_ID = gql`
	query Ride($id: ID!) {
		ride(id: $id) {
			id
			originalName
			slug
			intensity
			height
			length
			tempImage
			park {
				id
				originalName
				slug
			}
			rideCategories {
				id
				originalName
				color
			}
			rideTypes {
				id
				originalName
			}
		}
	}
`;

export const RIDES_QUERY_BY_ORIGINALNAME = gql`
	query Ride($filter: String!) {
		rides(filter: $filter) {
			id
			originalName
			slug
			park {
				id
				originalName
				slug
			}
		}
	}
`;

export const RIDES_QUERY = gql`
	query Ride {
		rides {
			id
			originalName
			slug
			tempImage
			park {
				id
				originalName
				slug
			}
			rideCategories {
				id
				originalName
				color
			}
			rideTypes {
				id
				originalName
			}
		}
	}
`;

export const RIDE_CATEGORY_BY_ID = gql`
	query RideCategory($id: ID!) {
		rideCategory(id: $id) {
			id
			originalName
			color
		}
	}
`;

export const RIDE_CATEGORIES_QUERY = gql`
	query RideCategories {
		rideCategories {
			id
			originalName
			color
			icon
		}
	}
`;

export const RIDE_TYPES_QUERY = gql`
	query RideTypes {
		rideTypes {
			id
			originalName
		}
	}
`;
