import { gql } from 'apollo-boost';

export const CURRENT_USER = gql`
	{
		me {
			id
			username
			email
			registerDate
			role
			parkVisits {
				id
				visitDate
				park {
					id
					originalName
					slug
					tempImage
				}
			}
			rideVisits {
				id
				visitDate
				ride {
					id
					originalName
					slug
					tempImage
					height
					length
					park {
						id
						slug
					}
					rideCategories {
						id
					}
				}
			}
			friendships {
				id
				createdAt
				requester {
					id
					username
				}
				addressee {
					id
					username
				}
			}
			friendshipRequests {
				id
				createdAt
				requester {
					id
					username
				}
			}
			achievementProgress {
				id
				isCompleted
				completedConditions
				achievement {
					id
					color
					icon
					translation(locale: "en") {
						id
						name
					}
					conditions {
						id
					}
				}
			}
		}
	}
`;

export const GET_USER_BY_ID = gql`
	query User($username: String!) {
		user(username: $username) {
			id
			username
			registerDate
			parkVisits {
				id
				visitDate
				park {
					id
					originalName
					slug
					tempImage
				}
			}
			rideVisits {
				id
				visitDate
				ride {
					id
					originalName
					slug
					tempImage
					park {
						id
						slug
					}
				}
			}
			achievementProgress {
				id
				isCompleted
				completedConditions
				achievement {
					id
					color
					icon
					translation(locale: "en") {
						id
						name
					}
					conditions {
						id
					}
				}
			}
		}
	}
`;
