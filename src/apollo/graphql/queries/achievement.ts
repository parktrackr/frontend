import { gql } from 'apollo-boost';

export const ACHIEVEMENTS_QUERY = gql`
	{
		achievements {
			id
			color
			icon
			conditions {
				id
				park {
					id
					originalName
				}
				ride {
					id
					originalName
				}
			}
			translation(locale: "en") {
				id
				name
			}
		}
	}
`;

export const ACHIEVEMENT_QUERY_BY_ID = gql`
	query Achievement($id: ID!) {
		achievement(id: $id) {
			id
			color
			icon
			translation(locale: "en") {
				id
				name
			}
			conditions {
				id
				park {
					id
					originalName
				}
				ride {
					id
					originalName
				}
			}
		}
	}
`;
