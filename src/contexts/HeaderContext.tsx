import React, { createContext, useReducer } from 'react';
import {
	titleReducer,
	subTitleReducer,
	headerColorReducer,
	subColorReducer,
	titleColorReducer,
	iconReducer,
	subHeaderReducer,
	titleActions,
	subTitleActions,
	headerColorActions,
	subColorActions,
	titleColorActions,
	iconActions,
	subHeaderActions,
} from '../reducers/headerReducer';
import emblem from '../assets/brand/trademark/emblem-white.png';

type InitialStateType = {
	title: string;
	subTitle: string | null;
	headerColor: string;
	subColor: string;
	titleColor: string;
	icon: string;
	subHeader: string | null;
};

const initialState = {
	title: '',
	subTitle: null,
	headerColor: '#FFFFFF',
	subColor: '#40C077',
	titleColor: '#000000',
	icon: emblem,
	subHeader: null,
};

export const HeaderContext = createContext<{
	state: InitialStateType;
	dispatch: React.Dispatch<any>;
}>({ state: initialState, dispatch: () => null });

const mainReducer = (
	{
		title,
		subTitle,
		headerColor,
		subColor,
		titleColor,
		icon,
		subHeader,
	}: InitialStateType,
	action:
		| titleActions
		| subTitleActions
		| headerColorActions
		| subColorActions
		| titleColorActions
		| iconActions
		| subHeaderActions
) => ({
	title: titleReducer(title, action),
	subTitle: subTitleReducer(subTitle, action),
	headerColor: headerColorReducer(headerColor, action),
	subColor: subColorReducer(subColor, action),
	titleColor: titleColorReducer(titleColor, action),
	icon: iconReducer(icon, action),
	subHeader: subHeaderReducer(subHeader, action),
});

const HeaderContextProvider: React.FC = ({ children }: any) => {
	const [state, dispatch] = useReducer(mainReducer, initialState);

	return (
		<HeaderContext.Provider value={{ state, dispatch }}>
			{children}
		</HeaderContext.Provider>
	);
};

export default HeaderContextProvider;
