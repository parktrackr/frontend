import React, { createContext, useReducer, useEffect } from 'react';
import { useQuery } from '@apollo/react-hooks';
import {
	loadingReducer,
	userReducer,
	tokenReducer,
	loadingActions,
	userActions,
	tokenActions,
	Types,
} from '../reducers/userReducer';
import { CURRENT_USER } from '../apollo/graphql/queries/user';

type User = {
	me: {
		id: string;
		username: string;
		parkVisits: any;
		rideVisits: any;
	};
};

type InitialStateType = {
	loading: boolean;
	token: string | null;
	currentUser?: User | any | null;
};

const initialState = {
	loading: true,
	token: null,
	currentUser: null,
};

export const UserContext = createContext<{
	state: InitialStateType;
	dispatch: React.Dispatch<any>;
}>({ state: initialState, dispatch: () => null });

const mainReducer = (
	{ loading, token, currentUser }: InitialStateType,
	action: userActions | tokenActions | loadingActions
) => ({
	loading: loadingReducer(loading, action),
	token: tokenReducer(token, action),
	currentUser: userReducer(currentUser, action),
});

const UserContextProvider: React.FC = ({ children }: any) => {
	const [state, dispatch] = useReducer(mainReducer, initialState);

	const { refetch } = useQuery(CURRENT_USER, {
		skip: !state.token,
		onCompleted(results) {
			dispatch({
				type: Types.Store,
				payload: {
					data: results,
				},
			});

			if (results === null) {
				dispatch({
					type: Types.ToggleLoading,
					payload: {
						bool: false,
					},
				});
			}
		},
		onError(err) {
			console.error(err);

			dispatch({
				type: Types.Load,
				payload: {
					token: null,
				},
			});
		},
	});

	useEffect(() => {
		if (localStorage.getItem('x-token')) {
			dispatch({
				type: Types.Load,
				payload: {
					token: localStorage.getItem('x-token'),
				},
			});
			refetch();
		}
		if (state.currentUser) {
			if (state.currentUser.me !== null) {
				dispatch({
					type: Types.ToggleLoading,
					payload: {
						bool: false,
					},
				});
			}
		}
	}, [state.token, state.currentUser, refetch]);

	return (
		<UserContext.Provider value={{ state, dispatch }}>
			{children}
		</UserContext.Provider>
	);
};

export default UserContextProvider;
