# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

- Nothing yet.

### Changed

## 0.1.1
### Added

 - Changelog

## 0.1.0
### Added
- Users can create accounts, log in and log out.
- Users can be made admin.
- Users have their own profile, which can be visited with their username.
- User profiles show their ride count, park count and achievements.
- Users can make use of a user dashboard.
- The user dashboard holds their ride count, park count, achievements, user information and friendships.
- Parks are their own entity and can be visited based on their slug.
- Parks can query their own rides.
- Rides are their own entity and can be visited based on their slug.
- Rides make use of Ride Categories, for categorizing.
- Rides make use of Ride Types, for further categorizing.
- Rides and Parks can be counted by a user.
- Rides and Parks make use of a tempImage, to show an image.
- Rides and Parks can be listed.
- Basic home page
- Basic contact page
- Users can earn Achievements for riding rides.
- There is an admin panel, where admins can alter with the data.
- Parks, Rides, Ride Categories, Ride Types, Achievements can be added, edited and deleted in the admin panel.